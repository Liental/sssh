import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CardHelper } from '@shared/helpers/card.helper';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { FileBrowserComponent } from '@shared/components/file-browser/file-browser.component';
import { first } from 'rxjs/operators';
import path from 'path';
import { InputModalComponent } from '@shared/modals/input-modal/input-modal.component';
import { AppService } from '@core/services/app.service';

@Component({
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PasswordComponent implements OnInit {
  public passphrase: string;
  public isShaking: boolean;

  public fileName: string;

  private loaderTimeout: NodeJS.Timeout;

  constructor(
    private notification: ToastrService,
    private modalService: BsModalService,
    private appService: AppService,
    private changeDetector: ChangeDetectorRef,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.fileName = localStorage.getItem('fileName');
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.appService.setTransparentTitlebar());
  }

  /** Change currently used configuration file. */
  public changeFile(): void {
    const currentFilePath = localStorage.getItem('filePath');
    const modal = this.modalService.show(FileBrowserComponent, { class: 'half-screen-higher' });

    if (currentFilePath) {
      modal.content.inputPath = currentFilePath;
      modal.content.usePath(true);
    }

    modal.content.handler.selectFiles$
      .pipe(first())
      .subscribe(file => {
        this.fileName = file[0].name;

        localStorage.setItem('filePath', modal.content.handler.getPath());
        localStorage.setItem('fileName', this.fileName);

        modal.hide();

        this.changeDetector.markForCheck();
      });
  }

  /** Create the configuration modal, once a new configuration is saved, update file data. */
  public newFile(): void {
    const inputModal = this.modalService.show(InputModalComponent, { class: 'fit-content' });

    inputModal.content.configurationSaved$
      .pipe(first())
      .subscribe(() => {
        this.notification.success('New configuration has been created!');
        this.passphrase = sessionStorage.getItem('passphrase');
        this.fileName = localStorage.getItem('fileName');

        this.loadData();
      });
  }

  /** Set passphrase in local storage and decrypt provided file with cards */
  public loadData(): void {
    sessionStorage.setItem('passphrase', this.passphrase);
    const savedPath = localStorage.getItem('filePath');
    const fileName = localStorage.getItem('fileName');
    const newPath = path.join(savedPath, fileName);
    const config = CardHelper.loadData(newPath, this.notification);

    if (config) {
      this.router.navigate(['home']);
      return;
    }

    this.isShaking = true;
    
    clearTimeout(this.loaderTimeout);
    setTimeout(() => {
      this.isShaking = false;
      this.changeDetector.markForCheck();
    }, 500);
  }
}
