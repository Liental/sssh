import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { HasPassphraseGuard } from '@core/guards/has-passphrase.guard';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [ HasPassphraseGuard ],
    canActivateChild: [ HasPassphraseGuard ]
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
