import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostListener, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ConnectionCard } from '@core/models/connection-card.model';
import { transition, style, animate, trigger, state } from '@angular/animations';
import { AppService } from '@core/services/app.service';
import { TerminalModel } from '@core/models/terminal.model';
import { TerminalService } from '@core/services/terminal.service';
import { TerminalGroup } from '@core/models/terminal-group.model';
import { ShortcutCard } from '@core/models/shortcut-card.model';
import { CardService } from '@core/services/card.service';
import { CommandCard } from '@core/models/command-card.model';
import { ICardPosition } from '@shared/interfaces/card-position.interface';
import { ShortcutCardComponent } from './components/shortcut-card/shortcut-card.component';
import { CardComponent } from './components/card/card.component';
import { CommandCardComponent } from './components/command-card/command-card.component';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('scaleDown', [
      state('in', style({ transform: 'scale(1)' })),

      transition(':enter', [
        style({ transform: 'scale(0)' }),
        animate(300)
      ]),

      transition(':leave', [
        animate(300, style({ transform: 'scale(0)' }))
      ])
    ]),
    trigger('slideIn', [
      state('in', style({ transform: 'translateX(0)' })),

      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate(300)
      ]),

      transition(':leave', [
        animate(300, style({ transform: 'translateX(-100%)' }))
      ]),
    ]),
    trigger('hideLeft', [
      state('true', style({ marginLeft: '-15rem' })),
      state('false', style({ marginLeft: '0rem' })),

      transition('true -> false', [
        style({ marginLeft: '-15rem' }),
        animate(300)
      ]),

      transition('false -> true', [
        style({ marginLeft: '0rem' }),
        animate(300)
      ])
    ])
  ]
})
export class HomeComponent {
  @ViewChildren('shortcutElem') shortcutElements: QueryList<ShortcutCardComponent>;
  @ViewChildren('commandElem') commandElements: QueryList<CommandCardComponent>;
  @ViewChildren('cardElem') cardElements: QueryList<CardComponent>;
  
  public selectedCard: ConnectionCard;
  public searchEnabled = false;
  
  private searchText: string;
  private subscription = new Subscription();

  constructor(
    private router: Router,
    public cardService: CardService,
    private appService: AppService,
    private terminalService: TerminalService,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit(): void {    
    this.cardService.loadData();
    
    // Turn off filtering when choosing a terminal group
    this.subscription.add(
      this.terminalService.changeTerminalGroup$.subscribe(terminalGroup => {
        this.searchText = '';
        this.searchEnabled = false;
      })
    );

    // Re-render cards when new data is loaded
    this.subscription.add(
      this.cardService.dataChanged$.subscribe(() => {
        this.changeDetector.markForCheck();
      })
    );

    // Re-render the component when a terminal group is changed
    this.subscription.add(
      this.terminalService.changeTerminalGroup$.subscribe(() => {
        this.changeDetector.detectChanges();
      })
    );

    this.changeDetector.markForCheck();
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.appService.setLightTitlebar());
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /** Enable searchbox filtering cards */
  @HostListener('document:keydown.control.f')
  public enableFilter(): void {
    if (this.selectedCard) {
      return;
    }

    this.searchEnabled = true;
  }
  
  /**
   * Retruns a list of connection cards from the card service.
   * If the `this.searchText` variable is specified, then filter the result by name.
   */
  public get cards(): ConnectionCard[] {
    if (this.searchEnabled && this.searchText) {
      return this.cardService.cards.filter(c => c.name.toLowerCase().includes(this.searchText.toLowerCase()));
    }

    return this.cardService.cards;
  }

  /**
   * Retruns a list of shortcuts from the card service.
   * If the `this.searchText` variable is specified, then filter the result by name.
   */
  public get shortcuts(): ShortcutCard[] {
    if (this.searchEnabled && this.searchText) {
      return this.cardService.shortcuts.filter(s => s.name.toLowerCase().includes(this.searchText.toLowerCase()));
    }

    return this.cardService.shortcuts;
  }

  /**
   * Retruns a list of commands from the card service.
   * If the `this.searchText` variable is specified, then filter the result by name.
   */
  public get commands(): CommandCard[] {
    if (this.searchEnabled && this.searchText) {
      return this.cardService.commands.filter(c => c.name.toLowerCase().includes(this.searchText.toLowerCase()));
    }
    
    return this.cardService.commands;
  }

  /**
   * Returns a list of terminal groups from the terminal service.
   */
  public get terminalGroups(): TerminalGroup[] {
    return this.terminalService.terminalGroups;
  };

  /**
   * Returns the currently selected group from the terminal service.
   */
  public get selectedGroup(): TerminalGroup {
    return this.terminalService.selectedGroup;
  };

  /**
   * Creates an external terminal with a ssh connection provided in the card model.
   * 
   * @param card model that contains informations about where should the terminal connect to.
   */
  public connectExternal(card: ConnectionCard): void {
    this.terminalService.openTerminal(card);
  }

  /**
   * Set filter for cards, shortcuts, and commands
   * 
   * @param text determines which cards should be displayed, they will be filtered by this parameter
   */
  public onSearch(text: string): void {
    this.searchText = text;
  }

  /**
   * Disable searchbar and reset it's content
   */
  public onSearchDisable(): void {
    this.searchText = null;
    this.searchEnabled = false;
  }

  /**
   * Switch the style theme between dark and light
   */
  public switchTheme(): void {
    this.appService.switchTheme();
  }

  /** Remove all connections and navigate back to passphrase component */
  public resetToPassphrase(): void {
    this.terminalService.terminalGroups = [];
    
    this.cardService.reset();
    this.appService.setLightTitlebar();
    
    this.router.navigateByUrl('passphrase');
  }

  /**
   * Checks wheter or not an element is positioned after provided position (x and y) 
   * 
   * @param element used to check the position
   * @param position x and y position used to compare the element's position to
   * @returns whether or not provided element is positioned after given position
   */
  private findElementIndex(element: { elementRef: ElementRef }, position: ICardPosition): boolean {
    const nativeElem = element.elementRef.nativeElement;
    const size = nativeElem.getBoundingClientRect();
    const isX = position.left >= size.x && position.left <= size.x + size.width;
    const isY = position.top >= size.y && position.top <= size.y + size.height;
    
    return isX && isY;
  }

  /**
   * Removes provided element from the provided position and places it in a 
   * new place in provided list
   * 
   * @param elementList generic list of cards (shortcut, command, connection)
   * @param element that will have it's position swaped
   * @param originalIndex where the element is placed before swaping it's position
   * @param newIndex where the element will be placed after swaping it's position
   */
  private swapPosition<T>(elementList: T[], element: T, originalIndex: number, newIndex: number): void {
    if (newIndex === -1) {
      newIndex = elementList.length - 1;
    }

    elementList.splice(originalIndex, 1);
    elementList.splice(newIndex, 0, element);
    
    this.cardService.saveData(false);

    // For some reason using `markForCheck` breaks connection cards.
    // The dragged card gets cloned onto the droppable card and everything breaks.
    this.changeDetector.detectChanges();
  }
  
  /**
   * Handles a drop event of a card, moves provided card to a new position 
   * based on the position that the card was dropped at.
   * 
   * @param shortcut card that just got dropped and will be placed in a new position
   * @param originalIndex where the element is placed before moving it to a new position
   * @param position x and y position where the element was dropped
   */
  public shortcutDropped(shortcut: ShortcutCard, originalIndex: number, position: ICardPosition): void {
    const elements = this.shortcutElements.toArray();
    const index = elements.findIndex(elem => this.findElementIndex(elem, position));

    this.swapPosition<ShortcutCard>(this.cardService.shortcuts, shortcut, originalIndex, index);
  }

  /**
   * Handles a drop event of a card, moves provided card to a new position 
   * based on the position that the card was dropped at.
   * 
   * @param command card that just got dropped and will be placed in a new position
   * @param originalIndex where the element is placed before moving it to a new position
   * @param position x and y position where the element was dropped
   */
  public commandDropped(command: CommandCard, originalIndex: number, position: ICardPosition): void {
    const elements = this.commandElements.toArray();
    const index = elements.findIndex(elem => this.findElementIndex(elem, position));

    this.swapPosition<CommandCard>(this.cardService.commands, command, originalIndex, index);
  }

  /**
   * Handles a drop event of a card, moves provided card to a new position 
   * based on the position that the card was dropped at.
   * 
   * @param card card that just got dropped and will be placed in a new position
   * @param originalIndex where the element is placed before moving it to a new position
   * @param position x and y position where the element was dropped
   */
  public connectionDropped(card: ConnectionCard, originalIndex: number, position: ICardPosition): void {
    const elements = this.cardElements.toArray();
    const index = elements.findIndex(elem => this.findElementIndex(elem, position));

    this.swapPosition<ConnectionCard>(this.cardService.cards, card, originalIndex, index);
  }
  
  /**
   * Creates a terminal group using selected card and focusses the application on it. 
   * The created terminal group will contain an internal terminal with a ssh connection.
   * When the new terminal group is created, this function will turn the titlebar color to match the terminal.
   * 
   * @param card model that contains information about where should the terminal connect to.
   * @param pwd optional path to navigate to at the beginning of the session
   * @param cmd optional command to execute at the beginning of the session
   */
  public connectCard(card: ConnectionCard, pwd?: string, cmd?: string): void {
    card.lastConnection = new Date();
    this.cardService.saveData(false);

    const terminal = new TerminalModel();
    terminal.card = Object.assign({}, card);
    terminal.isLocal = false;
    terminal.name = card.name.toString();
    terminal.pwd = pwd;
    terminal.cmd = cmd;

    this.terminalService.addGroupFromTerminal(terminal);
    this.appService.setDarkTitlebar();
  }

  /**
   * Creates a terminal group using selected card and focusses the application on it.
   * The created terminal group will contain a file browser connected to the SFTP server
   * along side a file browser with local files.
   * 
   * @param card model that contains information about where should the file browser connect to.
   */
  public openFileTransfer(card: ConnectionCard): void {
    const terminal = new TerminalModel();

    terminal.isLocal = false;
    terminal.isFileTransfer = true;
    terminal.name = card.name.toString();
    terminal.card = Object.assign({}, card);

    this.terminalService.addGroupFromTerminal(terminal);
    this.appService.setDarkTitlebar();
    
  }

  /**
   * Creates a new terminal that will execute provided script on the remote or local machine.
   * @param command a command definition contianing location and script to execute on the machine.
   */
  public executeCommand(command: CommandCard): void {
    if (command.connectionId) {
      this.connectCard(command.connection, command.location, command.command);
    } else {
      this.newLocalTerminal(command.location, command.command);
    }
  }
  
  /**
   * Creates a new external terminal with provided connection and location.
   * @param shortcut a shortcut definition containing desired connection and location to connect to.
   */
  public connectExternalShortcut(shortcut: ShortcutCard): void {
    this.terminalService.openTerminal(shortcut.connection, shortcut.location);
  }

  /**
   * Creates a new terminal group with provided connection and location.
   * @param shortcut a shortcut definition containing desired connection and location to connect to.
   */
  public connectShortcut(shortcut: ShortcutCard): void {
    shortcut.lastUsed = new Date();

    if (shortcut.connectionId) {
      this.connectCard(shortcut.connection, shortcut.location);
    } else {
      this.newLocalTerminal(shortcut.location);
    }
  }

  /**
   * Creates a terminal group with a local terminal and focusses the application on it.
   * When the new terminal group is created, this function will turn the titlebar color to match the terminal.
   * 
   * @param pwd optional path to start the terminal at
   * @param cmd optional command to execute on terminal startup
   */
  public newLocalTerminal(pwd?: string, cmd?: string): void {
    const terminal = new TerminalModel();
    terminal.pwd = pwd;
    terminal.cmd = cmd;
    terminal.isLocal = true;
    terminal.name = 'Terminal';

    this.terminalService.addGroupFromTerminal(terminal);
    this.appService.setDarkTitlebar();
  }
}
