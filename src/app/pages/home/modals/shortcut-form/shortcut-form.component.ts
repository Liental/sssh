import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { validate } from 'class-validator';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ShortcutCard } from '@core/models/shortcut-card.model';
import { ConnectionCard } from '@core/models/connection-card.model';

@Component({
  selector: 'app-shortcut-form',
  templateUrl: './shortcut-form.component.html',
  styleUrls: ['./shortcut-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShortcutFormComponent implements OnInit {
  @Input() card: ShortcutCard;
  @Input() connections: ConnectionCard[] = [];
  @Output() saveCard = new EventEmitter<ShortcutCard>();

  constructor(
    private notification: ToastrService,
    private modalRef: BsModalRef,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.card = new ShortcutCard();
  }
  
  ngAfterViewInit(): void {
    this.changeDetector.markForCheck();
  }
  
  public cancel(): void {
    this.modalRef.hide();
  }

  /** Save command using provided data and trim all fields */
  public async save(): Promise<void> {
    for (const key in this.card) {
      if (typeof this.card[key] === 'string') {
        this.card[key] = this.card[key].replace(/\s\s/gm, ' ').trim();
      }
    }
    
    const validated = await validate(Object.assign(new ShortcutCard(), this.card));

    if (validated.length) {
      const message = validated.map(val => Object.values(val.constraints).join('\r\n')).join('\r\n')      
      this.notification.error(message);
      return;
    }

    this.saveCard.emit(this.card);
    this.cancel();
  }
}
