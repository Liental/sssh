import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { validate } from 'class-validator';
import { ToastrService } from 'ngx-toastr';
import { ConnectionCard } from '@core/models/connection-card.model';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
  styleUrls: ['./card-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardFormComponent implements OnInit {
  @Input() card: ConnectionCard;
  @Output() saveCard = new EventEmitter<ConnectionCard>();

  /** Should the password field be covered or not */
  public show = false;

  constructor(
    private notification: ToastrService,
    private modalRef: BsModalRef,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.card = new ConnectionCard();
  }

  ngAfterViewInit(): void {
    this.changeDetector.markForCheck();
  }

  public cancelCard(): void {
    this.modalRef.hide();
  }

  public async save(): Promise<void> {
    for (const key in this.card) {
      if (typeof this.card[key] === 'string') {
        this.card[key] = this.card[key].replace(/\s\s/gm, ' ').trim();
      }
    }
    
    const validated = await validate(Object.assign(new ConnectionCard(), this.card));

    if (validated.length) {
      const message = validated.map(val => Object.values(val.constraints).join('\r\n')).join('\r\n')      
      this.notification.error(message);
      return;
    }

    this.saveCard.emit(this.card);
    this.cancelCard();
  }
}
