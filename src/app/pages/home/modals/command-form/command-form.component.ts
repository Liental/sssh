import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { validate } from 'class-validator';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ConnectionCard } from '@core/models/connection-card.model';
import { CommandCard } from '@core/models/command-card.model';

@Component({
  selector: 'app-command-form',
  templateUrl: './command-form.component.html',
  styleUrls: ['./command-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommandFormComponent implements OnInit {
  @Input() card: CommandCard;
  @Input() connections: ConnectionCard[] = [];
  @Output() saveCard = new EventEmitter<CommandCard>();

  constructor(
    private notification: ToastrService,
    private modalRef: BsModalRef,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.card = new CommandCard();
  }
  
  ngAfterViewInit(): void {
    this.changeDetector.markForCheck();
  }
  
  public cancel(): void {
    this.modalRef.hide();
  }

  /** Save command using provided data and trim all fields except for the command */
  public async save(): Promise<void> {
    for (const key in this.card) {
      if (typeof this.card[key] === 'string' && key !== 'command') {
        this.card[key] = this.card[key].replace(/\s\s/gm, ' ').trim();
      }
    }
    
    const validated = await validate(Object.assign(new CommandCard(), this.card));

    if (validated.length) {
      const message = validated.map(val => Object.values(val.constraints).join('\r\n')).join('\r\n')      
      this.notification.error(message);
      return;
    }

    this.saveCard.emit(this.card);
    this.cancel();
  }
}
