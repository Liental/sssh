import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown'

import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home.component';
import { SharedModule } from '@shared/shared.module';
import { CardFormComponent } from './modals/card-form/card-form.component';
import { CardComponent } from './components/card/card.component';
import { TerminalComponent } from './components/terminal/terminal.component';
import { TerminalListComponent } from './components/terminal-list/terminal-list.component';
import { TerminalGroupComponent } from './components/terminal-group/terminal-group.component';
import { ShortcutFormComponent } from './modals/shortcut-form/shortcut-form.component';
import { ShortcutCardComponent } from './components/shortcut-card/shortcut-card.component';
import { CommandFormComponent } from './modals/command-form/command-form.component';
import { CommandCardComponent } from './components/command-card/command-card.component';
import { FileTransferComponent } from './components/file-transfer/file-transfer.component';

@NgModule({
  declarations: [
    HomeComponent,
    CardFormComponent,
    ShortcutCardComponent,
    ShortcutFormComponent,
    CommandFormComponent,
    CardComponent,
    CommandCardComponent,
    TerminalComponent,
    TerminalListComponent,
    TerminalGroupComponent,
    FileTransferComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule,
    BsDropdownModule
    
  ]
})
export class HomeModule { }
