import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommandCard } from '@core/models/command-card.model';
import { CardService } from '@core/services/card.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-command-card',
  templateUrl: './command-card.component.html',
  styleUrls: ['./command-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommandCardComponent implements OnInit {
  @Input() card: CommandCard;
  @Output() deleteCard = new EventEmitter<CommandCard>();
  @Output() selectCard = new EventEmitter<CommandCard>();
  @Output() connectCard = new EventEmitter<CommandCard>();

  private subscription = new Subscription();
  
  constructor(
    public elementRef: ElementRef,
    private cardService: CardService,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(): void {
    this.load();
    this.changeDetector.detectChanges();

    this.subscription.add(this.cardService.dataChanged$.subscribe(() => this.load()));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private load(): void {
    this.card.connection = this.cardService.cards.find(c => c.uuid === this.card.connectionId);
    this.changeDetector.markForCheck();
  }
}
