import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostListener, OnInit, QueryList, ViewChildren } from '@angular/core';
import { TerminalGroup } from '@core/models/terminal-group.model';
import { TerminalModel } from '@core/models/terminal.model';
import { TerminalService } from '@core/services/terminal.service';
import { ICardPosition } from '@shared/interfaces/card-position.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-terminal-list',
  templateUrl: './terminal-list.component.html',
  styleUrls: ['./terminal-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TerminalListComponent implements OnInit {
  @ViewChildren('terminalGroupElem') terminalGroupElements: QueryList<ElementRef<HTMLDivElement>>;
  
  public isHidden = false;

  private subscription = new Subscription();
  
  constructor(
    private terminalService: TerminalService,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.subscription.add(
      this.terminalService.changeTerminalGroup$.subscribe(() => {
        this.changeDetector.detectChanges();
      })
    );

    this.subscription.add(
      this.terminalService.selectTerminal$.subscribe(() => {
        this.changeDetector.detectChanges();
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  
  public get terminalGroups(): TerminalGroup[] {
    return this.terminalService.terminalGroups;
  };

  public get selectedGroup(): TerminalGroup {
    return this.terminalService.selectedGroup;
  };

  @HostListener('window:keydown.control.shift.b')
  public switchHidden(): void {
    if (this.terminalGroups.length) {
      this.isHidden = !this.isHidden;
    }
  }

  /**
   * Find provided terminal and move it to another group based on it's x and y location.
   * Remove the terminal from previous group and focus the one where the terminal will be located.
   *  
   * @param groupIndex where the group is positioned in the group array
   * @param terminal terminal to find and move to another group
   * @param position x and y position of the terminal position
   */
  public swapTerminalGroup(groupIndex: number, terminal: TerminalModel, position: ICardPosition): void {
    const terminalGroupsArray = this.terminalGroupElements.toArray();

    const index = terminalGroupsArray.findIndex(element => {
      const size = element.nativeElement.getBoundingClientRect();
      return position.top >= size.y && position.top <= size.y + size.height;
    });

    if (index === -1 || groupIndex === index) {
      return
    }

    // Add terminal to a new group
    const newGroup = this.terminalGroups[index];
    const newColumn = newGroup.columns[newGroup.columns.length - 1];
    newColumn.terminals.push(terminal);

    // Remove terminal from previous group
    // The process has to be done in this order.
    // If the remove / add order is reversed and the origin group is above the
    // destination group, it would add the terminal to a different group or go out of bounds.
    const terminalGroup = this.terminalGroups[groupIndex];
    const columnIndex = terminalGroup.columns.findIndex(column => column.terminals.includes(terminal));

    const column = terminalGroup.columns[columnIndex];
    const terminalIndex = column.terminals.indexOf(terminal);

    column.terminals.splice(terminalIndex, 1);

    if (!column.terminals.length) {
      terminalGroup.columns.splice(columnIndex, 1);
    }

    if (!terminalGroup.columns.length) {
      this.terminalGroups.splice(groupIndex, 1);
    }

    const newGroupIndex = this.terminalGroups.indexOf(newGroup);
    this.terminalService.selectTerminalGroup(newGroupIndex);

    // For some reason `markForCheck` doesn't work on this one.
    // Same as with draggable cards.
    this.changeDetector.detectChanges();
  }

  /**
   * Loops through provided terminal group and extracts all terminals within.
   * 
   * @param terminalGroup used to extract children terminals.
   * @returns a list of terminal models in the whole group.
   */
  public terminalChildren(terminalGroup: TerminalGroup): TerminalModel[] {
    return !terminalGroup.columns.length ? [] : terminalGroup.columns
      .map(column => column.terminals)
      .reduce((previous, current) => previous.concat(current));
  }

  /**
   * Hide or show the side panel
   */
  public switchHide(): void {
    this.isHidden = !this.isHidden;
  }
  
  /**
   * Displays a terminal group on given index.
   * 
   * @param index determines which terminal group should be shown.
   */
  public selectTerminal(index: number): void {
    this.terminalService.selectTerminalGroup(index);
  }

  /**
   * Deletes a terminal group on given index.
   * 
   * @param index determines which terminal group should be deleted.
   */
  public deleteTerminal(index: number): void {
    this.terminalService.deleteTerminalGroup(index);
  }
}
