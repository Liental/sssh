import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ElectronService } from '@core/services';
import { ToastrService } from 'ngx-toastr';
import { Client, ClientChannel } from 'ssh2';
import * as xterm from 'xterm';
import { SearchAddon } from 'xterm-addon-search';
import { TerminalModel } from '@core/models/terminal.model';
import { FitAddon } from 'xterm-addon-fit';
import { WebLinksAddon } from 'xterm-addon-web-links'
import { ITerminal } from 'profoundjs-node-pty/src/interfaces';
import * as elementResizeDetectorMaker from 'element-resize-detector';
import { ArrowKey } from '@core/enums/arrow-key.enum';
import { CardService } from '@core/services/card.service';
import { CommandCard } from '@core/models/command-card.model';
import { ShortcutCard } from '@core/models/shortcut-card.model';
import { AppService } from '@core/services/app.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TerminalComponent {
  @ViewChild('terminalElem') terminalElement: ElementRef<HTMLDivElement>;

  @Input('data') data: TerminalModel;
  @Input('terminalId') terminalId: string;

  @Output('splitVert') splitVert = new EventEmitter();
  @Output('splitHoriz') splitHoriz = new EventEmitter();
  @Output('moveFocus') moveFocus = new EventEmitter<ArrowKey>();
  @Output('sessionEnded') sessionEnded = new EventEmitter();
  @Output('terminalSelected') terminalSelected = new EventEmitter();

  public searchEnabled = false;

  private detector: any;
  private detectorTimeout: NodeJS.Timeout;

  private terminal: xterm.Terminal;
  private searchAddon = new SearchAddon();
  private fitAddon = new FitAddon();
  private linksAddon: WebLinksAddon;

  private sshConnection: Client;
  private sshStream: ClientChannel;

  private process: ITerminal;
  private subscription = new Subscription();

  constructor(
    private electronService: ElectronService,
    private cardService: CardService,
    private appService: AppService,
    private notification: ToastrService,
    private elementRef: ElementRef,
    private changeDetector: ChangeDetectorRef
  ) { }

  /** Filter commands from configuration and return only the ones related to this terminal */
  public get commands(): CommandCard[] {
    return this.cardService.commands.filter(command => {
      if (!this.data.isLocal) {
        return command.connectionId === this.data.card.uuid;
      }

      return this.data.isLocal && !command.connectionId;
    });
  }

  /** Filter shortcuts from configuration and return only the ones related to this terminal */
  public get shortcuts(): ShortcutCard[] {
    return this.cardService.shortcuts.filter(shortcut => {
      if (!this.data.isLocal) {
        return shortcut.connectionId === this.data.card.uuid;
      }

      return this.data.isLocal && !shortcut.connectionId;
    });
  }
  
  ngAfterViewInit(): void {
    // Calls provided callback whenever an element resizes, used to resize xterm
    this.detector = elementResizeDetectorMaker({ strategy: 'scroll' });
    this.detector.listenTo(this.elementRef.nativeElement, () => {
      clearTimeout(this.detectorTimeout);
      this.detectorTimeout = setTimeout(this.fitTerminal.bind(this), 1000 / 30);
    });

    this.subscription.add(
      this.appService.changedTheme$.subscribe(isDark => {
        // I know.. it's deprecated.
        // The color is not refreshing using this.terminal.options, so I am using that instead.
        this.terminal.setOption('theme', { background: isDark ? '#111' : '#264564' })
      })
    );

    this.loadTerminal();

    if (this.data.isLocal) {
      this.loadLocalConsole();
    } else {
      this.loadSsh();
    }
  }

  ngOnDestroy(): void {
    this.detector.uninstall(this.elementRef.nativeElement);

    this.process && this.process.destroy();
    this.sshConnection && this.sshConnection.destroy();
    this.subscription.unsubscribe();
  }

  /**
   * Initializes the `node-pty` terminal and it's events so they pass into desired process. 
   */
  private loadTerminal(): void {
    this.terminal = new xterm.Terminal({
      theme: {
        background: this.appService.isDarkMode() ? '#111' : '#264564',
        foreground: 'white'
      }
    });

    this.linksAddon = new WebLinksAddon((event, uri) => {
      if (event.ctrlKey) {
        this.electronService.shell.openExternal(uri);
      }
    });

    this.terminal.loadAddon(this.searchAddon);
    this.terminal.loadAddon(this.linksAddon);
    this.terminal.loadAddon(this.fitAddon);
    this.terminal.open(this.terminalElement.nativeElement);
    
    // Add special key combinations
    this.terminal.attachCustomKeyEventHandler(event => {
      const ctrlShift = event.ctrlKey && event.shiftKey;

      if (!ctrlShift || event.type !== 'keyup') {
        return true;
      }

      switch (event.key) {
        case 'C':
          const data = this.terminal.getSelection();
          navigator.clipboard.writeText(data);

          break;
        case 'V':
          navigator.clipboard.readText().then(data => {
            if (!this.data.isLocal) {
              this.sshStream.write(data);
            } else {
              this.process.write(data);
            }
          });

          break;
        case 'F':
          this.searchEnabled = true;
          this.changeDetector.detectChanges();
          break;
        case 'G':
          this.terminal.clear();
          break;
        case 'O':
          this.splitHoriz.next();
          break;
        case 'E':
          this.splitVert.next();
          break;
        case 'ArrowLeft':
          this.moveFocus.next(ArrowKey.LEFT);
          break;
        case 'ArrowRight':
          this.moveFocus.next(ArrowKey.RIGHT);
          break;
        case 'ArrowUp':
          this.moveFocus.next(ArrowKey.UP);
          break;
        case 'ArrowDown':
          this.moveFocus.next(ArrowKey.DOWN);
          break;
      }

      event.preventDefault();
      event.stopPropagation();
      return false;
    });
    
    // Add key event handler
    this.terminal.onKey((keyEvent) => {
      if (this.data.isLocal) {
        this.process.write(keyEvent.key);
      } else if (this.sshStream) {
        this.sshStream.write(keyEvent.key);
      }
    });

    setTimeout(() => {
      this.fitTerminal()
      this.terminal.focus();
      this.terminalSelected.next();
    });
  }

  /**
   * Initializes a local shell process based on the system and hooks it up to `node-pty`.
   */
  private loadLocalConsole(): void {
    const isWindows = this.electronService.os.platform() === 'win32';
    const shell = isWindows ? 'powershell.exe' : process.env['SHELL'];

    this.process = this.electronService.nodePty.spawn(shell, [], {
      name: 'xterm-256color',
      cwd: (isWindows && this.data.pwd) || process.env.HOME
    });

    if (!isWindows && this.data.pwd) {
      this.process.write(`cd ${this.data.pwd}\r`);
    }

    if (this.data.cmd) {
      this.process.write(`${this.data.cmd}\r`);
    }

    this.process.on('data', (data) => {
      this.terminal.write(data);
    });

    this.process.on('exit', () => {
      this.sessionEnded.next();
    });
  }

  /**
   * Initializes an SSH connection based on the provided terminal card.
   * After initialization of the process, it hooks all the input / output data into `node-pty`.
   */
  private loadSsh(): void {
    this.sshConnection = new this.electronService.ssh2.Client();

    this.sshConnection.on('ready', () => {
      this.sshConnection.shell({ term: 'xterm-256color' }, (err, stream) => {
        if (err) {
          this.notification.error('Failed to send the command');
          return;
        }

        stream.on('data', (data: Buffer) => {
          let response = data.toString();
          this.terminal.write(response);
        });

        stream.on('exit', () => {
          this.sessionEnded.next();
        });

        this.sshStream = stream;

        if (this.data.pwd) {
          this.sshStream.write(`cd ${this.data.pwd}\r`);
        }

        if (this.data.cmd) {
          this.sshStream.write(`${this.data.cmd}\r`);
        }
        
        this.fitTerminal();
      });
    });

    this.sshConnection.connect({
      host: this.data.card.address,
      username: this.data.card.username,
      password: this.data.card.password,
      port: this.data.card.port,
    });
  }

  public useCommand(command: CommandCard): void {
    if (this.data.isLocal) {
      this.process.write(`cd ${command.location}\r`);
      this.process.write(`${command.command}\r`);
    } else {
      this.sshStream.write(`cd ${command.location}\r`);
      this.sshStream.write(`${command.command}\r`);
    }
  }

  public useShortcut(shortcut: ShortcutCard): void {
    if (this.data.isLocal) {
      this.process.write(`cd ${shortcut.location}\r`);
    } else {
      this.sshStream.write(`cd ${shortcut.location}\r`);
    }
  }

  /**
   * Finds and navigates to the next word found based on the provided input.
   * 
   * @param str text fragment to find in the terminal output.
   */
  public findNext(str: string): void {
    this.searchAddon.findNext(str);
  }

  /**
   * Finds and navigates to the previous word found based on the provided input.
   * 
   * @param str text fragment to find in the terminal output.
   */
  public findPrevious(str: string): void {
    this.searchAddon.findPrevious(str);
  }

  /**
   * Resizes the `node-pty` terminal to fit into it's parent.
   */
  public fitTerminal(): void {
    this.fitAddon.fit();

    const elem = this.elementRef.nativeElement;
    const proposed = this.fitAddon.proposeDimensions();
    
    if (!this.data.isLocal && this.sshStream && proposed) {
      this.sshStream.setWindow(
        proposed.rows,
        proposed.cols,
        elem.clientHeight,
        elem.clientWidth
      );
    } else if (this.data.isLocal && this.process && proposed) {
      this.process.resize(proposed.cols, proposed.rows);
    }
  }

  /**
   * Focuses the node-pty terminal.
   */
  public focus(): void {
    this.terminal.focus();
  }

  /** Open new command dialog with pre-selected connection */
  public newCommandForm(): void {
    const command = new CommandCard();

    if (!this.data.isLocal) {
      command.connectionId = this.data.card.uuid;
    }

    this.cardService.openCommandForm(command);
  }

  /** Open new shortcut dialog with pre-selected connection */
  public newShortcutForm(): void {
    const shortcut = new ShortcutCard();

    if (!this.data.isLocal) {
      shortcut.connectionId = this.data.card.uuid;
    }

    this.cardService.openShortcutForm(shortcut);
  }
}
