import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ShortcutCard } from '@core/models/shortcut-card.model';
import { CardService } from '@core/services/card.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shortcut-card',
  templateUrl: './shortcut-card.component.html',
  styleUrls: ['./shortcut-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShortcutCardComponent implements OnInit {
  @Input() card: ShortcutCard;
  @Output() deleteCard = new EventEmitter<ShortcutCard>();
  @Output() selectCard = new EventEmitter<ShortcutCard>();
  @Output() connectCard = new EventEmitter<ShortcutCard>();
  @Output() connectExternal = new EventEmitter<ShortcutCard>();

  private subscription = new Subscription();

  constructor(
    public elementRef: ElementRef,
    private cardService: CardService,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(): void {
    this.load();
    this.changeDetector.detectChanges();
    
    this.subscription.add(this.cardService.dataChanged$.subscribe(() => this.load()));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private load(): void {
    this.card.connection = this.cardService.cards.find(c => c.uuid === this.card.connectionId);
    this.changeDetector.markForCheck();
  }
}
