import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ArrowKey } from '@core/enums/arrow-key.enum';
import { TerminalColumn } from '@core/models/terminal-column.model';
import { TerminalGroup } from '@core/models/terminal-group.model';
import { TerminalModel } from '@core/models/terminal.model';
import { TerminalService } from '@core/services/terminal.service';
import { Subscription } from 'rxjs';
import { TerminalComponent } from '../terminal/terminal.component';

@Component({
  selector: 'app-terminal-group',
  templateUrl: './terminal-group.component.html',
  styleUrls: ['./terminal-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TerminalGroupComponent implements OnInit {
  @Input() data: TerminalGroup;
  @ViewChildren('terminalGroup') terminalGroups: QueryList<ElementRef<HTMLDivElement>>;
  @ViewChildren('terminalElement') terminalElements: QueryList<TerminalComponent>;

  public selectedTerminal: TerminalModel;

  private subscription = new Subscription();

  constructor(
    private terminalService: TerminalService,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.subscription.add(
      this.terminalService.changeTerminalGroup$.subscribe(() => {
        this.changeDetector.detectChanges();
      })
    );
  }

  ngAfterViewInit(): void {
    this.selectedTerminal = this.data.columns[0].terminals[0];
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * Finds a terminal component by provided instance data.
   * 
   * @param terminal terminal model to find the component by.
   * @returns a terminal component found by the provided data.
   */
  private findTerminal(terminal: TerminalModel): TerminalComponent {
    const column = this.data.columns.findIndex(column => column.terminals.includes(this.selectedTerminal));
    const terminals = this.data.columns[column].terminals;
    const row = terminals.indexOf(this.selectedTerminal);

    const columnElem = this.terminalGroups.get(column).nativeElement;
    const rowElem = columnElem.querySelectorAll('.terminal-container')[row].querySelector('.tt');
    const terminalElement = this.terminalElements.find(item => item.terminalId === rowElem['id'])

    return terminalElement;
  }

  /**
   * Move focus between terminals based on provided key.
   * 
   * @param key pressed button determining which way should the focus move towards
   */
  public moveFocus(key: ArrowKey): void {
    let column = this.data.columns.findIndex(column => column.terminals.includes(this.selectedTerminal));
    let terminals = this.data.columns[column].terminals;
    let row = terminals.indexOf(this.selectedTerminal);

    switch (key) {
      case ArrowKey.LEFT:
        column = column > 0 ? column - 1 : 0;
        terminals = this.data.columns[column].terminals;
        row = terminals.length - 1 >= row ? row : 0;
        break;
      case ArrowKey.RIGHT:
        column = column + 1 < this.data.columns.length ? column + 1 : column;
        terminals = this.data.columns[column].terminals;
        row = terminals.length - 1 >= row ? row : 0;
        break;
      case ArrowKey.UP:
        row = row > 0 ? row - 1 : 0;
        break;
      case ArrowKey.DOWN:
        row = row + 1 < terminals.length ? row + 1 : row;
        break;
    }

    const columnElem = this.terminalGroups.get(column).nativeElement;
    const rowElem = columnElem.querySelectorAll('.terminal-container')[row].querySelector('.tt');
    const terminal = this.terminalElements.find(item => item.terminalId === rowElem['id'])

    terminal.focus();
    this.selectTerminal(terminal.data);
  }

  /**
   * Closes connection to a terminal and deletes it from terminal list
   * 
   * @param terminal determines which terminal should be closed and deleted from current stack
   */
  public deleteTerminal(terminal: TerminalModel): void {
    const columnIndex = this.data.columns.findIndex(column => column.terminals.includes(terminal));
    const column = this.data.columns[columnIndex];
    const rowIndex = column.terminals.indexOf(terminal);

    column.terminals.splice(rowIndex, 1);

    if (!column.terminals.length) {
      this.data.columns.splice(columnIndex, 1);
    }

    this.terminalElements.first?.focus();
    this.selectTerminal(this.data.columns[0]?.terminals[0]);
    
    this.changeDetector.detectChanges();
  }

  /**
   * Moves focus to provided terminal
   * 
   * @param terminal determines which terminal should be selected
   */
  public selectTerminal(terminal: TerminalModel): void {
    this.selectedTerminal = terminal;
    this.terminalService.selectTerminal$.next(terminal);
  }

  /**
   * Vertically split current terminal group and create a new terminal in created column. 
   * Newly created terminal will be a copy of selected terminal.
   * 
   * @param localTerminal optional terminal to be copied instead of selected terminal
   */
  public splitColumns(localTerminal?: TerminalModel): void {
    const terminal = Object.assign(new TerminalModel(), localTerminal || this.selectedTerminal);
    const selectedColumn = this.data.columns.findIndex(c => c.terminals.includes(this.selectedTerminal));    
    const column = new TerminalColumn(terminal);

    terminal.cmd = '';

    this.data.columns.splice(selectedColumn + 1, 0, column);
    this.changeDetector.detectChanges();
  }

  /**
   * Horizontally split current terminal group and create a new terminal in created row. 
   * Newly created terminal will be a copy of selected terminal.
   * 
   * @param terminalModel optional terminal to be copied instead of selected terminal
   */
  public splitRows(terminalModel?: TerminalModel): void {
    const terminal = Object.assign(new TerminalModel(),  terminalModel || this.selectedTerminal);
    const column = this.data.columns.find(column => column.terminals.includes(this.selectedTerminal));
    const selectedRow = column.terminals.indexOf(this.selectedTerminal);

    terminal.cmd = '';

    column.terminals.splice(selectedRow + 1, 0, terminal);
    this.changeDetector.detectChanges();
  }
}
