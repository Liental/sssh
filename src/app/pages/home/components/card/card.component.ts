import { ChangeDetectorRef, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConnectionCard } from '@core/models/connection-card.model';
import { CardService } from '@core/services/card.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardComponent implements OnInit {
  @Input() card: ConnectionCard;
  @Output() deleteCard = new EventEmitter<ConnectionCard>();
  @Output() selectCard = new EventEmitter<ConnectionCard>();
  @Output() connectCard = new EventEmitter<ConnectionCard>();
  @Output() fileTransfer = new EventEmitter<ConnectionCard>();
  @Output() connectExternal = new EventEmitter<ConnectionCard>();

  private subscription = new Subscription();

  constructor(
    public elementRef: ElementRef,
    private cardService: CardService,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.subscription.add(
      this.cardService.dataChanged$.subscribe(() => this.changeDetector.markForCheck())
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
