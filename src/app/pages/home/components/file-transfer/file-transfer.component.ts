import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConnectionCard } from '@core/models/connection-card.model';
import { FileHelper } from '@shared/helpers/file.helper';
import { LocalFileBrowser } from '@shared/integration/file-browser/local-file-browser.integration';
import { FileModel } from '@shared/integration/file-browser/models/file.model';
import { TransferProgress } from '@shared/integration/file-browser/models/transfer-progress.model';
import { TransferFileModel } from '@shared/integration/file-browser/models/transfer-file.model';
import { SftpFileBrowser } from '@shared/integration/file-browser/sftp-file-browser.integration';
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-file-transfer',
  templateUrl: './file-transfer.component.html',
  styleUrls: ['./file-transfer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileTransferComponent implements OnInit {
  @Input() connection: ConnectionCard;

  @Output() exit = new EventEmitter<void>();
  @Output() addTerminal = new EventEmitter<void>();

  public isDirectionColumn = false;
  public localHandler: LocalFileBrowser;
  public sftpHandler: SftpFileBrowser;

  public uploading: TransferProgress[] = [];
  public downloading: TransferProgress[] = [];
  public waiting: TransferFileModel[] = [];

  public downloadsExpanded = true;
  public uploadsExpanded = true;
  public waitingExpanded = false;

  private subscription = new Subscription();

  constructor(
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.localHandler = new LocalFileBrowser();
    this.sftpHandler = new SftpFileBrowser(this.connection);

    this.subscription.add(
      this.sftpHandler.selectFiles$.subscribe(files => {
        files.forEach(file => file.isDirectory ? this.downloadFolder(file) : this.downloadFile(file))
        this.changeDetector.markForCheck();
      })
    );

    this.subscription.add(
      this.localHandler.selectFiles$.subscribe(files => {
        files.forEach(file => file.isDirectory ? this.uploadFolder(file) : this.uploadFile(file))
        this.changeDetector.markForCheck();
      })
    );

    this.subscription.add(
      interval(500).subscribe(() => {
        if (this.uploading.length || this.downloading.length) {
          this.changeDetector.detectChanges();
        }
      })
    );

    this.subscription.add(
      interval(200).subscribe(() => {
        const file = this.waiting[0];

        const startUploading = file?.direction === 'upload' && !this.uploading.length;
        const startDownloading = file?.direction === 'download' && !this.downloading.length;

        if (startUploading || startDownloading) {
          this.waiting.shift();

          startUploading && this.startUpload(file);
          startDownloading && this.startDownload(file);
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.sftpHandler.disconnect();
  }

  /**
   * Handle upload and download progress so it can be displayed on the lists.
   * When the transfer is completed, remove it's information from the list.
   * 
   * @param queue a list of current transfering files, either `uploadQueue` or `downloadQueue`
   * @param progress information about file transfer progress
   */
  private handleProgress(queue: TransferProgress[], progress: TransferProgress): void {
    let transfer = queue.find(model => model.id === progress.id);

    if (transfer) {
      transfer.transfered = progress.transfered;
    } else {
      transfer = progress;
      queue.push(transfer);
    }

    transfer.prc === 100 && this.handleTransferEnd(queue, transfer);
  }

  /**
   * Remove a file that just finished transfering from the queue.
   * 
   * @param queue where should the transfer be removed from
   * @param transfer transfer to remove from the queue
   */
  private handleTransferEnd(queue: TransferProgress[], transfer: TransferProgress): void {
    const transferIndex = queue.indexOf(transfer);
    queue.splice(transferIndex, 1);

    this.changeDetector.markForCheck();

  }

  /**
   * Start uploading provided file.
   * If the file is a folder, then create a folder and move on.
   * 
   * @param file used to extract path to transfer to the remote server
   */
  private async startUpload(file: TransferFileModel): Promise<void> {
    if (file.isDirectory) {
      await this.sftpHandler.mkdir(file.dest)
        .catch(err => console.log(`Failed to create ${file.dest}`));

      this.changeDetector.markForCheck();
      return;
    }

    this.sftpHandler.uploadFile(file.path, file.dest)
      .subscribe(progress => this.handleProgress(this.uploading, progress));
  }


  /**
   * Start downloading provided file.
   * If the file is a folder, then create a folder and move on.
   * 
   * @param file used to extract path to transfer to the local host
   */
  private startDownload(file: TransferFileModel): void {
    if (file.isDirectory) {
      try {
        FileHelper.mkdir(file.dest);
      } catch (e) {
        console.log(`Failed to create ${file.dest}`);
      }

      this.changeDetector.markForCheck();
      return;
    }

    this.sftpHandler.downloadFile(file.path, file.dest)
      .subscribe(progress => this.handleProgress(this.downloading, progress));
  }


  /** Switch the downloads section in the file progress box */
  public switchDownloads(): void {
    this.downloadsExpanded = !this.downloadsExpanded;
    this.changeDetector.detectChanges();
  }

  /** Switch the uploads section in the file progress box */
  public switchUploads(): void {
    this.uploadsExpanded = !this.uploadsExpanded;
    this.changeDetector.detectChanges();
  }

  /** Switch the waiting section in the file progress box */
  public switchWaiting(): void {
    this.waitingExpanded = !this.waitingExpanded;
    this.changeDetector.detectChanges();
  }

  /** Toggle direction in which the file browsers are displayed */
  public toggleDirection(): void {
    this.isDirectionColumn = !this.isDirectionColumn;
    this.changeDetector.detectChanges();
  }

  /**
   * Upload a **regular** file to the remote host based on provided informations.
   * 
   * @param file used to get local and remote path of the file
   */
  public uploadFile(file: FileModel): void {
    const remotePath = this.sftpHandler.getFilePath(file, true);
    const localPath = this.localHandler.getFilePath(file);

    const uploadFile = TransferFileModel.fromFileModel(file)

    uploadFile.direction = 'upload';
    uploadFile.path = localPath;
    uploadFile.dest = remotePath;

    this.waiting.push(uploadFile);
  }

  /**
   * Upload a folder to the remote host based on provided informations.
   * 
   * @param folder used to get local and remote path of the folder
   */
  public async uploadFolder(folder: FileModel): Promise<void> {
    const localPath = this.localHandler.getFilePath(folder);
    const remotePath = this.sftpHandler.getFilePath(folder, true);

    const files = FileHelper
      .readdirRecursive(localPath)
      .map(file => {
        const parsedLocalPath = this.localHandler.getFilePath(file);
        const parsedRemotePath = parsedLocalPath.replace(localPath, remotePath);
        const uploadFile = TransferFileModel.fromFileModel(file);

        uploadFile.direction = 'upload';
        uploadFile.path = parsedLocalPath;
        uploadFile.dest = parsedRemotePath;

        return uploadFile;
      });

    this.waiting.push(...files);
  }

  /**
   * Download a **regular** file from the remote host based on provided informations.
   * 
   * @param file used to get local and remote path of the file
   */
  public downloadFile(file: FileModel): void {
    const remotePath = this.sftpHandler.getFilePath(file);
    const localPath = this.localHandler.getFilePath(file, true);

    const downloadFile = TransferFileModel.fromFileModel(file)

    downloadFile.direction = 'download';
    downloadFile.path = remotePath;
    downloadFile.dest = localPath;

    this.waiting.push(downloadFile);
  }

  /**
   * Download a folder from the remote host based on provided infromations.
   * 
   * @param folder used to get local and remote path of the folder
   */
  public async downloadFolder(folder: FileModel): Promise<void> {
    const localPath = this.localHandler.getFilePath(folder, true);
    const remotePath = this.sftpHandler.getFilePath(folder);

    const files = await this.sftpHandler.readdirRecursive(remotePath);
    const transferFiles = files.map(file => {
      const parsedRemotePath = this.sftpHandler.getFilePath(file);
      const parsedLocalPath = parsedRemotePath.replace(remotePath, localPath);
      const downloadFile = TransferFileModel.fromFileModel(file);

      downloadFile.direction = 'download';
      downloadFile.path = parsedRemotePath;
      downloadFile.dest = parsedLocalPath;

      return downloadFile;
    });

    this.waiting.push(...transferFiles);
  }
}
