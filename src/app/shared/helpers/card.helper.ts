import { ConfigData } from '@core/models/config-data.model';
import { ToastrService } from 'ngx-toastr';
import { CryptoHelper } from './crypto.helper';
import { FileHelper } from './file.helper';

export class CardHelper {
  public static loadData(path: string, notification: ToastrService): ConfigData {
    try {
      const encryptedData = FileHelper.read(path);
      const decryptedData = CryptoHelper.decrypt(encryptedData);

      if (decryptedData?.length) {
        const obj = JSON.parse(decryptedData);        
        return Object.assign(new ConfigData(), obj);
      }

      notification.error('Failed to load the configuration file');
    } catch (e: any) {
      console.error(e);
      notification.error('Failed to load the configuration file');
    }
  }
}