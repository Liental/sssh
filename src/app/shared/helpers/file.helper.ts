import { FileModel } from '@shared/integration/file-browser/models/file.model';
import * as FsType from 'fs';
import { Dirent, Stats } from 'fs';
import path from 'path';

const fs: typeof FsType = window.require('fs');
export class FileHelper {
  public static exists(path: string): boolean {
    return fs.existsSync(path);
  }

  public static mkdir(path: string): void {
    return fs.mkdirSync(path);
  }

  public static readdir(path: string): Dirent[] {
    return fs.readdirSync(path, { withFileTypes: true });
  }

  public static read(path: string): string {
    return fs.readFileSync(path).toString();
  }

  public static write(path: string, data: string): void {
    return fs.writeFileSync(path, data);
  }

  public static statSync(path: string): Stats {
    return fs.statSync(path);
  }

  public static delete(path: string): void {
    return fs.rmSync(path, { recursive: true, force: true });
  }

  public static rename(path: string, newPath: string): void {
    return fs.renameSync(path, newPath);
  }

  /**
   * Recursively extract files from a path and all it's children folders.
   * 
   * @param path folder used to extract the files from
   * @returns a list of files in provided path
   */
  public static readdirRecursive(path: string): FileModel[] {
    const files = FileHelper.readdir(path);
    const result: FileModel[] = [];

    const currentFolder = new FileModel();
    currentFolder.path = path;
    currentFolder.isDirectory = true;

    result.push(currentFolder);

    for (const file of files) {
      if (file.isDirectory()) {
        const newFiles = FileHelper.readdirRecursive(`${path}/${file.name}`);
        result.push(...newFiles);
      } else if (file.isFile()) {
        result.push(FileModel.fromLocalFile(file, path));
      }
    }

    return result.sort((a, b) => a.isDirectory && !b.isDirectory ? -1 : 1);
  }

  /** Create a pretty human readable version of a file size */
  public static prettyPrintSize(size: number): string {
    const kilobytes = size / 1024;
    const megabytes = kilobytes / 1000;
    const gigabytes = megabytes / 1000;

    let value = kilobytes;
    let suffix = 'KB';

    if (gigabytes >= 1) {
      value = gigabytes;
      suffix = 'GB';
    } else if (megabytes >= 1) {
      value = megabytes;
      suffix = 'MB';
    }

    return `${value.toFixed(2)} ${suffix}`;
  }

  /**
   * Joins provided string using `path` package and then replaces slashes
   * in the result string
   * 
   * @param str a set of strings used to compose a path 
  */
  public static composePath(...str: string[]) {
    return path.join(...str).split('\\').join('/');
  }

}