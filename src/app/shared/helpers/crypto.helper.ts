import { AES, enc as Encodings } from 'crypto-js';

export class CryptoHelper {
  public static encrypt(data: string): string {
    return AES.encrypt(data, sessionStorage.getItem('passphrase')).toString();
  }
  
  public static decrypt(data: string): string {
    return AES.decrypt(data, sessionStorage.getItem('passphrase')).toString(Encodings.Utf8);
  }
}