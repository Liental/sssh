export enum FileSort {
  NAME = 'name',
  SIZE = 'size',
  DATE = 'modifyDate'
}

export enum FileSortDirection {
  ASC = -1,
  DESC = 1
}
