import { Directive, ElementRef, EventEmitter, Input, NgZone, Output } from '@angular/core';
import { ICardPosition } from '@shared/interfaces/card-position.interface';

@Directive({ selector: '[dragElement]' })
export class DragDropDirective {
  @Input() multiplierY = 1.5;
  @Input() multiplierX = 2;
  @Input() dropElementParent: HTMLDivElement;

  @Output() elementDropped = new EventEmitter<ICardPosition>();
  @Output() elementDragged = new EventEmitter<ICardPosition>();

  constructor(private elementRef: ElementRef, private ngZone: NgZone) {
    // Run outside of angular so the change detection doesn't trigger
    // every time a cursor moves
    this.ngZone.runOutsideAngular(() => {
      this.elementRef.nativeElement.addEventListener('dragstart', (event: DragEvent) => this.dragElementStart(event));
      this.elementRef.nativeElement.addEventListener('drag', (event: DragEvent) => this.dragElement(event));
      this.elementRef.nativeElement.addEventListener('dragend', (event: DragEvent) => this.dragElementDrop(event));
    });
  }

  /**
   * Calculates the position of currently dragged element and returns it's coordinates.
   * 
   * @param event drag event used to extract the mouse position
   * @param target native element that is being dragged, used to calculate width and height
   * @returns a position of currently dragged element
   */
  private getPosition(event: DragEvent, target: HTMLDivElement): ICardPosition {
    const size = target.getBoundingClientRect();

    return {
      width: size.width,
      height: size.height,
      top: event.clientY - size.height / this.multiplierY,
      left: event.clientX - size.width / this.multiplierX
    }
  }

  /**
   * Find a droppable element that is currently being hovered over and add highlight to it.
   * Remove highlight from every other droppable elements.
   * 
   * @param event used to calculate currently active element
   */
  private addHighlight(event: DragEvent): void {
    const droppableElements = this.dropElementParent.getElementsByClassName('ng-drop-element');
    const nativeElem = this.elementRef.nativeElement as HTMLDivElement;
    const children = Array.from(nativeElem.children);

    for (const element of Array.from(droppableElements)) {
      const size = element.getBoundingClientRect();
      const isX = event.clientX >= size.x && event.clientX <= size.x + size.width;
      const isY = event.clientY >= size.y && event.clientY <= size.y + size.height;

      if (isX && isY && !children.includes(element)) {
        element.classList.add('ng-drop-element-active');
      } else {
        element.classList.remove('ng-drop-element-active');
      }
    }
  }

  /** Remove active drop hightlight from all droppable elements */
  private removeHightlight(): void {
    const droppableElements = this.dropElementParent.getElementsByClassName('ng-drop-element');

    for (const element of Array.from(droppableElements)) {
      element.classList.remove('ng-drop-element-active');
    }
  }

  /**
   * Disable ghost preview of the element while dragging
   * 
   * @param event `dragstart` event data
   */
  public dragElementStart(event: DragEvent): void {
    if (!this.elementRef.nativeElement.draggable) {
      event.preventDefault();
      event.stopPropagation();
      return;
    }

    const img = new Image();
    img.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=';

    event.dataTransfer.setDragImage(img, 0, 0);

    this.elementRef.nativeElement.classList.add('ng-drag-element-dragging');
  }

  /**
   * Start dragging process, make the element stick to the mouse.
   * If `dropElementParent` is provided, find an element that matches 
   * cursor position and highlight it.
   * 
   * @param event `drag` event data
   */
  public dragElement(event: DragEvent): void {
    if (event.clientX === 0 && event.clientY === 0) {
      return;
    }

    const target = this.elementRef.nativeElement;
    const position = this.getPosition(event, target);

    this.dropElementParent && this.addHighlight(event);

    const y = position.top;
    const x = position.left;

    target.style.zIndex = 9999;
    target.style.pointerEvents = 'none';
    target.style.position = 'absolute';
    target.style.transform = `translateY(${y}px) translateX(${x}px)`;

    target.style.top = 0;
    target.style.left = 0;

    this.elementDragged.next(position);
  }

  /**
   * End dragging process, make the element go back to normal position and 
   * send an `elementDropped` event to the host.
   * 
   * If `dropElementParent` was provided, then remove all highlight effects
   * from droppable elements.
   * 
   * @param event `dragend` event data
   */
  public dragElementDrop(event: DragEvent): void {
    const target = this.elementRef.nativeElement;

    const position = this.getPosition(event, target);
    const size = {
      left: event.clientX,
      top: event.clientY,
      width: position.width,
      height: position.height
    };

    this.elementDropped.next(size);

    target.style.position = 'relative';
    target.style.zIndex = 1;
    target.style.top = 'unset';
    target.style.left = 'unset';
    target.style.transform = 'unset';
    target.style.pointerEvents = 'unset';

    target.classList.remove('ng-drag-element-dragging');
    this.dropElementParent && this.removeHightlight();
  }
}
