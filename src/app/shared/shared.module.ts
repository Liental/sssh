import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { FormsModule } from '@angular/forms';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { DragDropDirective } from './directives/drag-element/drag-element.directive';
import { FileBrowserComponent } from './components/file-browser/file-browser.component';
import { InputModalComponent } from './modals/input-modal/input-modal.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
  declarations: [DragDropDirective, SearchbarComponent, FileBrowserComponent, InputModalComponent],
  imports: [CommonModule, TranslateModule, FormsModule, BsDropdownModule],
  exports: [TranslateModule, DragDropDirective, FormsModule, SearchbarComponent, FileBrowserComponent, InputModalComponent]
})
export class SharedModule {}
