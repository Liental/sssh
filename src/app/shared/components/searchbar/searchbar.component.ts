import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit {
  @ViewChild('search') searchElem: ElementRef<HTMLInputElement>;

  @Input() noArrows = false;
  
  @Output() disable = new EventEmitter<boolean>();
  @Output() find = new EventEmitter<string>();
  @Output() findNext = new EventEmitter<string>();
  @Output() findPrevious = new EventEmitter<string>();

  public currentContent: string;
  public changeEvent$ = new Subject<string>();
  
  constructor() { }

  ngOnInit(): void {
    this.changeEvent$
      .pipe(debounceTime(300))
      .subscribe(content => this.find.next(content));
  }

  ngAfterViewInit() {
    this.searchElem.nativeElement.focus();
  }

  public disableSearchbar(event: Event): void {
    this.disable.next(true);
  }
}
