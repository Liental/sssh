import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ConnectionCard } from '@core/models/connection-card.model';
import { ShortcutCard } from '@core/models/shortcut-card.model';
import { CardService } from '@core/services/card.service';
import { FileSort, FileSortDirection } from '@shared/enums/file-sort.enum';
import { BaseFileBrowser } from '@shared/integration/file-browser/base-file-browser.integration';
import { LocalFileBrowser } from '@shared/integration/file-browser/local-file-browser.integration';
import { FileModel } from '@shared/integration/file-browser/models/file.model';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-file-browser',
  templateUrl: './file-browser.component.html',
  styleUrls: ['./file-browser.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileBrowserComponent implements OnInit {
  @ViewChild('newFolderName') newFolderName: ElementRef<HTMLInputElement>;
  
  @Input() inputPath: string;
  @Input() handler: BaseFileBrowser = new LocalFileBrowser();
  @Input() hideTitle: boolean;
  @Input() title: string;
  @Input() selectFileTitle: string;
  @Input() selectFolderTitle: string;

  /** Whether to allow selection of currently used directory via button */
  @Input() allowCurrentDirectory: boolean;

  /** Whether selecting multiple files is allowed */
  @Input() allowMultiple: boolean;

  /** Used to filter shortcuts so one can quickly move to a different path */
  @Input() data: ConnectionCard;

  /** Event that's called when a folder is clicked, returns new path */
  @Output() exit = new EventEmitter<void>()

  public sortFields = [
    { icon: 'font', name: 'Sort by name', field: FileSort.NAME },
    { icon: 'balance-scale', name: 'Sort by size', field: FileSort.SIZE },
    { icon: 'calendar', name: 'Sort by date', field: FileSort.DATE }
  ];

  public sortDirections = [
    { icon: 'sort-amount-down', name: 'Sort descending', dir: FileSortDirection.DESC },
    { icon: 'sort-amount-up', name: 'Sort ascending', dir: FileSortDirection.ASC }
  ];

  public isList = true;
  public addingFolder = false;
  public searchEnabled = false;

  public files: FileModel[] = [];
  public selectedFiles: FileModel[] = [];

  private subscription = new Subscription();

  constructor(
    private cardService: CardService,
    private notification: ToastrService,
    private changeDetector: ChangeDetectorRef
  ) { }

  /** Should the quick access folder button be shown */
  public get showFolder(): boolean {
    return this.cardService.isLoaded;
  }

  /** Filter shortcuts from configuration and return only the ones related to this connection */
  public get shortcuts(): ShortcutCard[] {
    return this.cardService.shortcuts.filter(shortcut => {
      const isLocal = !this.data;

      if (!isLocal) {
        return shortcut.connectionId === this.data.uuid;
      }

      return isLocal && !shortcut.connectionId;
    });
  }

  /**
   * Checks if there are any observers on events.
   * If there are observers on any subject, then display the footer.
   */
  public get displayFooter(): boolean {
    return !!this.handler.selectFiles$.observers.length || !!this.handler.selectFolder$.observers.length;
  }

  ngOnInit(): void {
    this.subscription.add(
      this.handler.dataChanged$.subscribe(() => this.loadHandlerData())
    );
  }

  ngAfterViewInit(): void {
    this.loadHandlerData();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /** Update files and file path */
  private loadHandlerData(): void {
    this.selectedFiles = [];
    this.files = this.handler.getFiles();
    this.inputPath = this.handler.getPath();
    this.changeDetector.detectChanges();
  }

  /** Enable searchbox filtering cards */
  @HostListener('keydown.control.f')
  private enableFilter(): void {
    this.searchEnabled = true;
  }

  /** Select all files */
  @HostListener('keydown.control.a', ['$event'])
  private allEvent(event: KeyboardEvent): void {
    this.returnInput(event) && this.selectAll();
  }

  /** Delete selected files */
  @HostListener('keyup.delete', ['$event'])
  private deleteEvent(event: KeyboardEvent): void {
    this.returnInput(event) && this.deleteFiles();
  }

  /** Enable folder adding */
  @HostListener('keyup.control.n', ['$event'])
  private addEvent(event: KeyboardEvent): void {
    this.returnInput(event) && this.enableNewFolder();
  }

  /**
   * Checks whether provided event is targeted on an input.
   * If an input is the target, then return false and do nothing,
   * otherwise cancel the event and return true
   *  
   * @param event fired to check whether the target is an input or not
   */
  private returnInput(event: Event): boolean {
    if (event.target instanceof HTMLInputElement) {
      return false;
    }

    event.preventDefault();
    return true;
  }

  /**
   * Add or remove a file from selected files list.
   * 
   * @param file to add or remove from the list
  */
  private ctrlSelection(file: FileModel): void {
    const fileIndex = this.selectedFiles.indexOf(file);

    if (fileIndex > -1) {
      this.selectedFiles.splice(fileIndex, 1);
    } else {
      this.selectedFiles.push(file);
    }
  }

  /**
   * Add all files in range from the last selected file to provided file to selection.
   * 
   * @param file to determine where should the selection end
   */
  private shiftSelection(file: FileModel): void {
    const fileIndex = this.files.indexOf(file);
    const lastIndex = this.files.indexOf(this.selectedFiles[this.selectedFiles.length - 1]);
    const sorted = [fileIndex, lastIndex].sort((a, b) => a > b ? 1 : -1);
    const files = this.files.slice(sorted[0], sorted[1] + 1);

    for (const file of files) {
      !this.selectedFiles.includes(file) && this.selectedFiles.push(file);
    }
  }

  /** Select all files */
  public selectAll(): void {
    this.selectedFiles = this.files;
  }

  /**
   * Create a form used to create a new folder
   */
  public enableNewFolder(): void {
    this.addingFolder = true;
    this.changeDetector.detectChanges();
    this.newFolderName.nativeElement.focus();
  }

  /**
   * Create a folder with given name and refresh the view
   * 
   * @param name of the folder that's meant to be created
   */
  public addNewFolder(name: string): void {
    this.handler.createFolder(name)
      .then(() => {
        this.addingFolder = false;
        this.usePath(true)
      });
  }

  /**
   * Determine if selected file should be added to already selected files
   * or if it should replace currently selected list.
   * 
   * @param file that will be added to current selection
   */
  public selectFile(file: FileModel, event: MouseEvent): void {
    if (event?.ctrlKey && this.allowMultiple) {
      this.ctrlSelection(file);
      return;
    }

    if (event?.shiftKey && this.allowMultiple) {
      this.shiftSelection(file);
      return;
    }

    this.selectedFiles = [file];
  }

  /** Toggle file view between list view and box view */
  public toggleList(): void {
    this.isList = !this.isList;
    this.changeDetector.detectChanges();
  }

  /** Emit current path to all `selectPath$` observers */
  public selectPath(): void {
    const fileModel = new FileModel();

    fileModel.name = this.selectedFiles[0]?.name;
    fileModel.path = this.handler.getPath();
    fileModel.isDirectory = true;

    if (this.allowCurrentDirectory && !this.selectedFiles[0]?.isDirectory) {
      fileModel.name = '.';
      this.handler.selectFolder$.next(fileModel);
      return;
    }

    this.handler.selectFolder$.next(fileModel);
  }

  /**
   * Filter files in currently used path by their name.
   * 
   * @param name used to filter the files by name
   */
  public onSearch(name: string): void {
    this.files = this.handler
      .getFiles()
      .filter(file => file.name.toLowerCase().includes(name.toLowerCase()));
  }

  /** Restore the filter when the searchbar is closed */
  public onSearchDisable(): void {
    this.files = this.handler.getFiles();
    this.searchEnabled = false;
  }

  /**
   * Go to a previously saved location 
   * 
   * @param shortcut where should the file browser go to
   */
  public useShortcut(shortcut: ShortcutCard): void {
    this.inputPath = shortcut.location;
    this.usePath();
  }

  /**
   * Navigate to specified path and add it to navigation history.
   * 
   * @param skipHistory if `true`, then do not add new path to navigation history
   */
  public async usePath(skipHistory?: boolean): Promise<void> {
    try {
      await this.handler.usePath(this.inputPath, skipHistory);
    } catch (e) {
      this.inputPath = this.handler.getPath();
      this.notification.error('Failed to navigate to provided path');
    }
  }

  /** Open a form to create a new shortcut */
  public newShortcutForm(): void {
    const shortcut = new ShortcutCard();

    shortcut.connectionId = this.data?.uuid || '';
    shortcut.location = this.inputPath.toString();

    this.cardService.openShortcutForm(shortcut);
  }

  /**
   * Basaed on provided file, check whether it is a directory and the path should be replaced,
   * or if it's a regular file and shoould it be uploaded.
   * 
   * @param file to check what whether the path should be replaced, or the file uploaded
   */
  public handleDdblClick(file: FileModel): void {
    if (file.isDirectory) {
      this.handler.usePath(file.composedPath);
      return;
    }

    this.handler.handleFile(file);
  }

  /** Delete selected files */
  public async deleteFiles(): Promise<void> {
    const failedFiles = [];

    for (const file of this.selectedFiles) {
      await this.handler.deleteFile(file)
        .catch(e => failedFiles.push({ error: e.message || e, file }));
    }

    this.usePath(true);
  }

  /**
   * Rename selected file
   *
   * @param file to rename
   * @param name to be given to the file 
   */
  public rename(file: FileModel, name: string): void {
    file.renaming = false;
    
    this.handler.renameFile(file, name)
      .then(() => this.usePath(true));
  }

  /**
   * Set file to renaming mode, detect changes to create the input
   * and focus newly created input
   * 
   * @param file to rename 
   * @param element to focus
   */
  public enableRenaming(file: FileModel, element: HTMLDivElement): void {
    file.renaming = true;
    this.changeDetector.detectChanges();

    const inputElement = element.children[0] as HTMLInputElement;
    inputElement?.focus();
  }

  /** Handle a file click */
  public handleFile(): void {
    this.handler.handleFile(this.allowMultiple ? this.selectedFiles : this.selectedFiles[0]);
  }
}
