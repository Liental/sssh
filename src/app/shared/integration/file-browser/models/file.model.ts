import { FileHelper } from '@shared/helpers/file.helper';
import { Dirent } from 'fs';
import { FileInfo } from 'ssh2-sftp-client'; 
import { BaseFileBrowser } from '../base-file-browser.integration';

export class FileModel {
  name: string;
  path: string;
  isDirectory: boolean;
  size?: number;
  modifyDate?: Date;
  renaming?: boolean;

  constructor () {
    this.size = 0;
    this.modifyDate = new Date(0);
  }

  /** Create a pretty human readable version of the file size */
  public get prettyFileSize(): string {
    return FileHelper.prettyPrintSize(this.size);
  }
  
  /** Create an absolute path composed of the file path and the file name */
  public get composedPath(): string {
    return FileHelper.composePath(this.path, this.name);
  }

  /**
   * Create a new file model based on provided local file.
   * 
   * **More extended informations such as size and date are not included here**
   * 
   * @param file fs file model that is used to fill in basic file information
   * @param filePath path of the local file
   * @returns a filled in file model with basic informations
   */
  public static fromLocalFile(file: Dirent, filePath?: string): FileModel {
    const fileModel = new FileModel();

    fileModel.name = file.name;
    fileModel.path = filePath;
    fileModel.isDirectory = file.isDirectory();
    
    return fileModel;
  }

  /**
   * Create a new file model based on provided remote file.
   * 
   * @param file sftp file model that is used to fill in extended file information
   * @param filePath path of the remote file
   * @returns a failled in file model with extended file information
   */
  public static fromSftpFile(file: FileInfo, filePath?: string): FileModel {
    const fileModel = new FileModel();

    fileModel.name = file.name;
    fileModel.path = filePath;
    fileModel.size = file.size;
    fileModel.isDirectory = file.type === 'd';
    fileModel.modifyDate = new Date(file.modifyTime);

    return fileModel;
  }
}