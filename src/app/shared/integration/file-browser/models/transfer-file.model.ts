import { FileModel } from './file.model';

export class TransferFileModel {
  path: string;
  dest: string;
  size?: number;
  isDirectory: boolean;
  direction: 'upload' | 'download';

  /**
   * Create a file transfer model
   * 
   * @param file model that is going to be transfered
   * @returns a new instance of TransferFileModel with all information filled in
   */
  public static fromFileModel(file: FileModel): TransferFileModel {
    const newModel = new TransferFileModel();

    newModel.size = file.size;
    newModel.isDirectory = file.isDirectory;
    newModel.path = `${file.path}/${file.name}`;
    
    return newModel;
  }
}
