import { FileHelper } from '@shared/helpers/file.helper';

export class TransferProgress {
  id: string;
  path: string;
  size: number
  transfered: number;
  destination: string;

  /** Progress representend in percentage */
  public get prc(): number {
    return +((this.transfered / this.size) * 100).toFixed(2);
  }

  /** Create a pretty human readable version of the file size */
  public get prettyFileSize(): string {
    return FileHelper.prettyPrintSize(this.size);
  }

  /** Create a pretty human readable version of the file size */
  public get prettyTransfered(): string {
    return FileHelper.prettyPrintSize(this.transfered);
  }
}
