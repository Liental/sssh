import { FileHelper } from '@shared/helpers/file.helper';
import { BaseFileBrowser } from './base-file-browser.integration';
import { FileModel } from './models/file.model';

export class LocalFileBrowser extends BaseFileBrowser {
  constructor() {
    super();
    this.init();
  }

  /** Load home folder */
  private init(): void {
    this.usePath(process.env.HOME);
  }

  public async renameFile(file: FileModel, name: string): Promise<void> {
    FileHelper.rename(file.composedPath, FileHelper.composePath(file.path, name));
  }

  public async createFolder(name: string): Promise<void> {
    FileHelper.mkdir(FileHelper.composePath(this.path, name));
  }

  public async deleteFile(file: FileModel): Promise<void> {
    FileHelper.delete(file.composedPath);
  }
  
  public async usePath(newPath: string, skipHistory?: boolean): Promise<FileModel[]> {
    this.files = FileHelper
      .readdir(newPath)
      .filter(file => file.isFile() || file.isDirectory())
      .map(file => {        
        const fileModel = FileModel.fromLocalFile(file, newPath)
        const stats = FileHelper.statSync(newPath + '/' + file.name);

        fileModel.size = stats.size;
        fileModel.modifyDate = stats.mtime;

        return fileModel;
      });

    return super.usePath(newPath, skipHistory);
  }
}