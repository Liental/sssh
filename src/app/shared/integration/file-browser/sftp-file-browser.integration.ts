import { BaseFileBrowser } from './base-file-browser.integration';
import * as sftp from 'ssh2-sftp-client';
import { ConnectionCard } from '@core/models/connection-card.model';
import { FileModel } from './models/file.model';
import { TransferProgress } from './models/transfer-progress.model';
import { Subject } from 'rxjs';
import { v4 } from 'uuid';
import { FileHelper } from '@shared/helpers/file.helper';

const Client: typeof sftp = window.require('ssh2-sftp-client');

export class SftpFileBrowser extends BaseFileBrowser {
  private client: sftp;
  private connection: ConnectionCard;

  constructor(connection: ConnectionCard) {
    super();

    this.connection = connection;
    this.init();
  }

  /** Initialize an SFTP connection with a remote host */
  private init(): void {
    this.client = new Client();

    this.client.connect({
      host: this.connection.address,
      port: this.connection.port,
      username: this.connection.username,
      password: this.connection.password
    }).then(async () => {
      const path = await this.client.cwd();
      this.path = path;
      this.usePath(path, true);
    });
  }

  public renameFile(file: FileModel, name: string): Promise<string> {
    return this.client.rename(file.composedPath, FileHelper.composePath(file.path, name));
  }

  public createFolder(name: string): Promise<string> {
    return this.client.mkdir(FileHelper.composePath(this.path, name));
  }
  
  public deleteFile(file: FileModel): Promise<string> {
    const path = FileHelper.composePath(file.path, file.name);
    
    if (file.isDirectory) {
      return this.client.rmdir(path, true);
    } else {
      return this.client.delete(path);
    }
  }

  public async usePath(newPath: string, skipHistory?: boolean): Promise<FileModel[]> {
    const files = await this.client.list(newPath);

    this.files = files
      .map(file => FileModel.fromSftpFile(file, newPath));

    return super.usePath(newPath, skipHistory);
  }

  /** Disconnect the SFTP connection */
  public disconnect(): void {
    this.client.end();
  }

  /**
   * Recursively extract files from a path and all it's children folders.
   * 
   * @param path folder used to extract the files from
   * @returns a list of files in provided path
   */
   public async readdirRecursive(path: string): Promise<FileModel[]> {
    const files = await this.client.list(path);
    const result: FileModel[] = [];

    const currentFolder = new FileModel();
    currentFolder.path = path;
    currentFolder.isDirectory = true;
    
    result.push(currentFolder);

    for (const file of files) {
      if (file.type === 'd') {
        const newFiles = await this.readdirRecursive(`${path}/${file.name}`);
        result.push(...newFiles);
      } else {
        result.push(FileModel.fromSftpFile(file, path));
      }
    }

    return result.sort((a, b) => a.isDirectory && !b.isDirectory ? -1 : 1);
  }


  /** 
   * Create a directory on the remote host
   *
   * @param dest path to the new directory 
   */
  public mkdir(dest: string): Promise<string> {
    return this.client.mkdir(dest);
  }

  /**
   * Upload a **regular** file from the local machine to the SFTP connection target
   * 
   * @param path which local file should be uploaded
   * @param dest what remote path and name should the uploaded file have
   * @returns observable that will push new data when there is any progress on the file
   */
  public uploadFile(path: string, dest: string): Subject<TransferProgress> {
    const subject = new Subject<TransferProgress>();
    const transferId = v4();
    
    this.client.fastPut(path, dest, {
      step: (trans: number, chunk: number, total: number) => {
        const progress = new TransferProgress();

        progress.path = path;
        progress.size = total;
        progress.id = transferId;
        progress.destination = dest;
        progress.transfered = trans;

        subject.next(progress);
      }
    }).catch(err => subject.error(err));
    
    return subject;
  }

  /**
   * Upload a folder from the local machine to the SFTP connection target
   * 
   * @param path which local folder should be uploaded
   * @param dest what remote path and name should the uploaded folder have
   * @returns result of the upload operation
   */
  public async uploadFolder(path: string, dest: string): Promise<string | NodeJS.WritableStream | Buffer> {
    return this.client.uploadDir(path, dest);
  }

  /**
   * Download a **regular** file from the SFTP connection to the local machine
   * 
   * @param path which remote file should be downlaoded
   * @param dest what local path and name should the downlaoded file have
   * @returns observable that will push new data when there is any progress on the file
   */
  public downloadFile(path: string, dest: string): Subject<TransferProgress> {
    const subject = new Subject<TransferProgress>();
    const transferId = v4();
    
    this.client.fastGet(path, dest, {
      step: (trans: number, chunk: number, total: number) => {
        const progress = new TransferProgress();

        progress.path = path;
        progress.size = total;
        progress.id = transferId;
        progress.destination = dest;
        progress.transfered = trans;

        subject.next(progress);
      }
    }).catch(err => subject.error(err));
    
    return subject;
  }

  /**
   * Download a folder from the SFTP connection to the local machine
   * 
   * @param path which remote folder should be downlaoded
   * @param dest what local path and name should the downlaoded folder have
   * @returns result of the download operation
   */
  public downloadFolder(path: string, dest: string): Promise<string | NodeJS.WritableStream | Buffer> {
    return this.client.downloadDir(path, dest)
  }
}