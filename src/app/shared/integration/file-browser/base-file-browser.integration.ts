import { FileSort, FileSortDirection } from '@shared/enums/file-sort.enum';
import { FileHelper } from '@shared/helpers/file.helper';
import { Subject } from 'rxjs';
import { FileModel } from './models/file.model';

export abstract class BaseFileBrowser {
  protected path: string;
  protected files: FileModel[];

  private history: string[] = [];

  private _sort: FileSort = FileSort.NAME;
  private _sortDir: FileSortDirection = FileSortDirection.ASC;

  /** Event that's called when data changes */
  public dataChanged$ = new Subject<void>();

  /** Event that's called when a regular file is clicked */
  public selectFiles$ = new Subject<FileModel[]>();

  /** Event that's called when a folder is clicked */
  public selectFolder$ = new Subject<FileModel>();

  /** Whether or not the file browser can go to previous location */
  public get canGoBack(): boolean {
    return !!this.history.length;
  }

  /** Active sort field */
  public get sort(): FileSort {
    return this._sort;
  }

  /** Active sort direction */
  public get sortDir(): FileSortDirection {
    return this._sortDir;
  }
  
  /**
   * Add a new entry in navigation history.
   * 
   * @param path used to add a new entry in the navigation history
   */
   protected addHistory(path: string): void {
    path && this.history.push(path);
  }

  /**
   * Remove last element from navigation history and return it.
   * 
   * @returns the last place that the file browser was navigated to
   */
  protected popHistory(): string {
    return this.history.pop();
  }
  
  /**
   * Set sorting direction that is used to sort the files.
   * 
   * @param direction which direction should the files be sorted in
   */
  public setSortDir(direction: FileSortDirection): void {
    this._sortDir = direction;
    this.sortFiles();
  }

  /**
   * Sort current files by given field and set the handler to automatically sort by it.
   * 
   * @param sortBy optional field used to define which value should the files be sorted by
   */
  public sortFiles(sortBy?: FileSort): void {
    if (sortBy) {
      this._sort = sortBy;
    }

    this.files = this.files
      .sort((a, b) => {
        const isName = this._sort === FileSort.NAME;
        const valueA = isName ? a.name.toLowerCase() : a[this._sort];
        const valueB = isName ? b.name.toLowerCase() : b[this._sort];

        return valueA < valueB ? this._sortDir : -this._sortDir;
      })
      .sort((a, b) => a.isDirectory && !b.isDirectory ? -1 : 1);
  }

  /**
   * Navigate to specified path and add it to navigation history.
   * Sort files in current directory by their name and type.
   * 
   * @param newPath where should the file browser navigate to
   * @param skipHistory if `true`, then do not add new path to navigation history
   */
  public async usePath(newPath: string, skipHistory?: boolean): Promise<FileModel[]> {
    !skipHistory && this.history[this.history.length - 1] !== newPath && this.addHistory(this.path);

    this.sortFiles();
    this.path = newPath;
    this.dataChanged$.next();

    return this.files;
  }

  /**
   * Returns a copy of currently used path of the file browser.
   * 
   * @returns currently used path of the file browser
   */
  public getPath(): string {
    return this.path ? FileHelper.composePath(this.path.toString()) : '';
  }

  /**
   * Create a path based on provided file.
   * This function will replace any `\` character with `/`.
   * 
   * @param file used to extract file path and name
   * @param useCurrentPath optional parameter that determines whether to use current path instead of file path
   * @returns a composed path of the file
   */
  public getFilePath(file: FileModel, useCurrentPath?: boolean): string {
    const basePath = useCurrentPath ? this.path : file.path;
    return file.name ? FileHelper.composePath(basePath, file.name) : basePath;
  }

  /**
   * Returns a list of files in currently used folder.
   * 
   * @returns a list of files in currently used folder
   */
  public getFiles(): FileModel[] {
    return this.files;
  }

  /** Refresh current directory, update the files */
  public refreshData(): void {
    this.usePath(this.path, true);
  }

  /** Navigate to the last place in current navigation history */
  public goBack(): void {
    const newPath = this.popHistory();
    newPath && this.usePath(newPath, true);
  }

  /** Go to the parent folder of current path */
  public goUp(): void {
    const newPath = FileHelper.composePath(this.path, '..');
    this.usePath(newPath);
  }

  /**
   * Handle click event on a file in the file browser.
   * Usually files are either regular files or folders. 
   * 
   * @param files containing information about clicked files
   */
  public handleFile(files: FileModel | FileModel[]): void {
    if (files instanceof FileModel && files.isDirectory) {
      const newPath = FileHelper.composePath(this.path, files.name);
      this.usePath(newPath);
      return;
    }

    this.selectFiles$.next(files instanceof FileModel ? [ files ] : files);
  }

  /**
   * Delete provided file from the file system
   * 
   * @param file to remove from the system
  */
  public async deleteFile(file: FileModel): Promise<void | any> {
    console.log('This method is not yet implemented');
  }

  /**
   * Rename provided file with a new name
   * 
   * @param file to rename
   * @param name to be given to the file 
  */
   public async renameFile<T>(file: FileModel, name: string): Promise<void | any> {
    console.log('This method is not yet implemented');
  }

  /**
   * Create a folder in current directory with provided name
   * 
   * @param name to use while creating the folder
   */
  public async createFolder(name: string): Promise<void | any> {
    console.log('This method is not yet implemented');
  }
}