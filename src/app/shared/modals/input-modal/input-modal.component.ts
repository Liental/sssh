import { Component, OnInit } from '@angular/core';
import { ConfigData } from '@core/models/config-data.model';
import { FileBrowserComponent } from '@shared/components/file-browser/file-browser.component';
import { CryptoHelper } from '@shared/helpers/crypto.helper';
import { FileHelper } from '@shared/helpers/file.helper';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import path from 'path';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-input-modal',
  templateUrl: './input-modal.component.html',
  styleUrls: ['./input-modal.component.scss']
})
export class InputModalComponent implements OnInit {
  public passphrase = '';
  public fileName = 'sssh-config';
  public filePath = process.env.HOME;

  public configurationSaved$ = new Subject<void>();

  constructor(
    private modalService: BsModalService,
    private notification: ToastrService,
    private modalRef: BsModalRef
  ) { }

  ngOnInit(): void {

  }

  /** Hide the modal. */
  public exit(): void {
    this.modalRef.hide();
  }
  
  /** Use filled in data in order to encrypt and save new configuration file. */
  public save(): void {
    if (this.passphrase.length === 0) {
      this.notification.error('The passphrase cannot be empty!');
      return;
    }
    
    sessionStorage.setItem('passphrase', this.passphrase);

    const config = new ConfigData();
    const encrypted = CryptoHelper.encrypt(JSON.stringify(config));
    const newPath = path.join(this.filePath, this.fileName);

    try {
      FileHelper.write(newPath, encrypted);

      localStorage.setItem('filePath', this.filePath);
      localStorage.setItem('fileName', this.fileName);

      this.configurationSaved$.next();
      this.exit();
    } catch (e) {
      console.error(e);
      this.notification.error(`Failed to save the configuration file`);
    }
  }

  /** Open file browser in order to select a path for new configuration file. */
  public openBrowser(): void {
    const modal = this.modalService.show(FileBrowserComponent, {
      class: 'half-screen-higher',
      initialState: { allowCurrentDirectory: true }
    });

    modal.content.handler.selectFolder$
      .pipe(first())
      .subscribe(file => {
        this.filePath = path.join(file.path, file.name);
        modal.hide();
      });
  }
}
