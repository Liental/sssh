export class ICardPosition {
  height: number;
  width: number;
  top: number;
  left: number;
}
