import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeRoutingModule } from '@pages/home/home-routing.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'password',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'password'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }),
    HomeRoutingModule,
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
