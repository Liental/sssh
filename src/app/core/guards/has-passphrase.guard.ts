import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HasPassphraseGuard implements CanActivate {
  constructor (private router: Router) {

  }
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const hasPassphrase = !!sessionStorage.getItem('passphrase');

      if (!hasPassphrase) {
        sessionStorage.clear();
        this.router.navigate([ 'passphrase' ]);
      }
      
      return hasPassphrase;
  }

}
