export enum ArrowKey {
  LEFT,
  RIGHT,
  UP,
  DOWN
}