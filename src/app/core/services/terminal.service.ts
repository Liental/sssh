import { Injectable } from '@angular/core';
import { ConnectionCard } from '@core/models/connection-card.model';
import { TerminalGroup } from '@core/models/terminal-group.model';
import { TerminalModel } from '@core/models/terminal.model';
import { FileHelper } from '@shared/helpers/file.helper';
import { Subject } from 'rxjs';
import { ElectronService } from '.';
import { AppService } from './app.service';

@Injectable({ providedIn: 'root' })
export class TerminalService {
  public terminalGroups: TerminalGroup[] = [];
  public selectedGroup: TerminalGroup;

  public changeTerminalGroup$ = new Subject<TerminalGroup>();
  public selectTerminal$ = new Subject<TerminalModel>();

  constructor(private appService: AppService, private electronService: ElectronService) { }

  /**
   * Open an external terminal window using provided card or at the home location.
   * 
   * @param card optional connection card used to define the ssh command,
   * if none provided then open a local window.
   */
  public openTerminal(card?: ConnectionCard, pwd?: string): void {
    const isWindows = this.electronService.os.platform() === 'win32';
    let cmd = `${isWindows ? 'powershell.exe -noexit -Command' : process.env['SHELL'] + ' -c'} "ssh ${card?.username}@${card?.address} -p ${card?.port}"`;

    if (!card) {
      cmd = isWindows ? 'powershell.exe' : process.env['SHELL'];

      if (pwd) {
        pwd = FileHelper.composePath(pwd);
        cmd += isWindows ? ` -noexit -command "cd ${pwd}"` : ` -c "cd ${pwd}"`;
      }
    }

    this.electronService.childProcess.spawn(cmd, {
      detached: true,
      shell: true
    });
  }

  /**
   * Creates a terminal group using an already created terminal model.
   * 
   * @param terminal model that should be added into the created terminal group.
   */
  public addGroupFromTerminal(terminal: TerminalModel): void {
    const terminalGroup = new TerminalGroup(terminal);
    this.addTerminalGroup(terminalGroup);
  }

  /**
   * Inserts a group of terminals into the group list.
   * 
   * @param group a group of terminals that should be added into the list.
   */
  public addTerminalGroup(group: TerminalGroup): void {
    this.terminalGroups.push(group);
    this.selectedGroup = group;
    this.changeTerminalGroup$.next(this.selectedGroup);
  }

  /**
   * Removes a terminal group from the lsit using the specified index.
   * If after this operation no groups are left in the list, change the titlebar colors to the default one.
   * 
   * @param index defines which terminal group should be deleted from the list.
   */
  public deleteTerminalGroup(index: number): void {
    if (this.terminalGroups[index] === this.selectedGroup) {
      this.selectedGroup = null;
    }

    this.terminalGroups.splice(index, 1);

    if (!this.terminalGroups.length) {
      this.appService.setLightTitlebar();
    }
    
    this.changeTerminalGroup$.next(this.selectedGroup);
  }

  /**
   * Sets a termianl group at the provided index as currently actvie.
   * 
   * @param index defines which terminal group should be selected as currently active.
   */
  public selectTerminalGroup(index: number): void {
    this.selectedGroup = this.terminalGroups[index];
    this.changeTerminalGroup$.next(this.selectedGroup);
  }
}
