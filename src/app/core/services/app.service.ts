import { Injectable } from '@angular/core';
import { Titlebar } from 'custom-electron-titlebar';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AppService {
  private titlebar: Titlebar;
  private isDark: boolean = false;
  
  public changedTheme$ = new Subject<boolean>();

  constructor() {
    window.addEventListener('load', () => {
      this.titlebar = window['titleBar'];
    });

    this.isDark = !!(+localStorage.getItem('isDark'));
  }

  public isDarkMode(): boolean {
    return this.isDark;
  }

  public switchTheme(): void {
    this.isDark = !this.isDark;

    const darkStr = this.isDark as any >> 0;
    const htmlElem = document.querySelector('html');

    if (this.isDark) {
      htmlElem.classList.add('dark');
    } else {
      htmlElem.classList.remove('dark');
    }
    
    localStorage.setItem('isDark', darkStr.toString());
    this.changedTheme$.next(this.isDark);
  }

  public setTitle(title: string): void {
    this.titlebar.updateTitle(title);
  }

  public setDarkTitlebar(): void {
    // document.querySelector('.titlebar').classList.remove('light');
    document.querySelector('.titlebar').classList.remove('custom-transparent');
    document.querySelector('.titlebar').classList.add('custom-dark');
  }

  public setTransparentTitlebar(): void {
    // document.querySelector('.titlebar').classList.remove('light');
    document.querySelector('.titlebar').classList.remove('custom-dark');
    document.querySelector('.titlebar').classList.add('custom-transparent');
  }
  
  public setLightTitlebar(): void {
    document.querySelector('.titlebar').classList.remove('custom-transparent');
    document.querySelector('.titlebar').classList.remove('custom-dark');
    // document.querySelector('.titlebar').classList.add('light');
  }
}
