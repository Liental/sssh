import { Injectable } from '@angular/core';
import { CommandCard } from '@core/models/command-card.model';
import { ConnectionCard } from '@core/models/connection-card.model';
import { ShortcutCard } from '@core/models/shortcut-card.model';
import { CardFormComponent } from '@pages/home/modals/card-form/card-form.component';
import { CommandFormComponent } from '@pages/home/modals/command-form/command-form.component';
import { ShortcutFormComponent } from '@pages/home/modals/shortcut-form/shortcut-form.component';
import { CardHelper } from '@shared/helpers/card.helper';
import { CryptoHelper } from '@shared/helpers/crypto.helper';
import { FileHelper } from '@shared/helpers/file.helper';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import path from 'path';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';
import { v4 } from 'uuid';

@Injectable({ providedIn: 'root' })
export class CardService {
  public cards: ConnectionCard[] = [];
  public shortcuts: ShortcutCard[] = [];
  public commands: CommandCard[] = [];

  public dataChanged$ = new Subject<void>();

  private filePath: string;
  private _isLoaded: boolean;

  constructor(
    private modalService: BsModalService,
    private notification: ToastrService,
  ) {}

  /** Is the configuration loaded */
  public get isLoaded(): boolean {
    return this._isLoaded;
  }
  
  /** Load configuration data using `localStorage` paths. */
  public loadData(): void {
    if (!localStorage.getItem('filePath') || !localStorage.getItem('fileName')) {
      this.notification.error('No path to configuration file was specified');
      return;
    }

    const filePath = localStorage.getItem('filePath');
    const fileName = localStorage.getItem('fileName');

    this.filePath = path.join(filePath, fileName);

    const data = CardHelper.loadData(this.filePath, this.notification);

    if (data) {
      this.cards = data.connections || [];
      this.shortcuts = data.shortcuts || [];
      this.commands = data.commands || [];
      
      this._isLoaded = true;
      this.dataChanged$.next();
    }
  }

  /** Clear all configurations */
  public reset(): void {
    this.cards = [];
    this.commands = [];
    this.shortcuts = [];
    this._isLoaded = false;
  }

  /**
   * Deletes a card from the list and saves the file on the local filesystyem.
   * 
   * @param card entry that should be deleted from the card list.
   */
  public deleteCard(card: ConnectionCard): void {
    const index = this.cards.findIndex(c => c.uuid === card.uuid);

    this.shortcuts.forEach(shortcut => {
      if (shortcut.connectionId === card.uuid) {
        shortcut.connectionId = null;
        shortcut.connection = null;
      }
    });

    this.commands.forEach(command => {
      if (command.connectionId === card.uuid) {
        command.connectionId = null;
        command.connection = null;
      }
    });

    this.cards.splice(index, 1);
    this.saveData();
  }

  /**
   * Deletes a command from the list and saves the file on the local filesystyem.
   * 
   * @param command entry that should be deleted from the command list.
   */
  public deleteCommand(command: CommandCard): void {
    const index = this.commands.findIndex(c => c.uuid === command.uuid);

    this.commands.splice(index, 1);
    this.saveData();
  }

  /**
   * Deletes a card from the list and saves the file on the local filesystyem.
   * 
   * @param shortcut entry that should be deleted from the card list.
   */
  public deleteShortcut(shortcut: ShortcutCard): void {
    const index = this.shortcuts.findIndex(c => c.uuid === shortcut.uuid);

    this.shortcuts.splice(index, 1);
    this.saveData();
  }

  /**
   * Saves the card list into a a file on the local filesystem.
   * 
   * @param notification defines if the `success` notifcication should be displayed on a successful save.
   * Defaults to `true`.
   */
  public saveData(notification: boolean = true): void {
    try {
      const commands = this.commands.map(command => {
        const newCommand = Object.assign(new CommandCard(), command);
        newCommand.connection = null;
        return newCommand;
      });

      const shortcuts = this.shortcuts.map(s => {
        const newShortcut = Object.assign(new ShortcutCard(), s);
        newShortcut.connection = null;
        return newShortcut;
      });

      const data = { commands, shortcuts, connections: this.cards };
      const encryptedData = CryptoHelper.encrypt(JSON.stringify(data));
      
      FileHelper.write(this.filePath, encryptedData);
      this.dataChanged$.next();

      if (notification) {
        this.notification.success('Successfully updated the configuration file');
      }
    } catch (e: any) {
      console.error(e);
      this.notification.error('Failed to update the configuration file');
    }

    this.dataChanged$.next();
  }

  /**
   * Inserts provided card into the card list and then saves the list to a file on the filesystem.
   * If a card with the same `uuid` already exists, it updates the already existing one instead of inserting a new model.
   * 
   * @param card model that should be inserted into the card list
   */
  public async saveCard(card: ConnectionCard): Promise<void> {
    const presentIndex = this.cards.findIndex(c => c.uuid === card.uuid);
    const newCard = Object.assign({}, card);

    if (presentIndex > -1) {
      this.cards[presentIndex] = Object.assign(this.cards[presentIndex], newCard);
    } else {
      newCard.uuid = v4();
      this.cards.push(newCard);
    }

    this.saveData();
  }

  /**
   * Inserts provided shortcut into the shortcut list and then saves the list to a file on the filesystem.
   * If a shortcut with the same `uuid` already exists, it updates the already existing one instead of inserting a new model.
   * 
   * @param shortcut model that should be inserted into the shortcut list
   */
  public async saveShortcut(shortcut: ShortcutCard): Promise<void> {
    const presentIndex = this.shortcuts.findIndex(s => s.uuid === shortcut.uuid);
    const newShortcut = Object.assign({}, shortcut);

    newShortcut.connection = this.cards.find(c => c.uuid === newShortcut.connectionId);

    if (presentIndex > -1) {
      this.shortcuts[presentIndex] = Object.assign(this.shortcuts[presentIndex], newShortcut);
    } else {
      newShortcut.uuid = v4();
      this.shortcuts.push(newShortcut);
    }

    this.saveData();
  }

  /**
   * Inserts provided command into the command list and then saves the list to a file on the filesystem.
   * If a command with the same `uuid` already exists, it updates the already existing one instead of inserting a new model.
   * 
   * @param command model that should be inserted into the command list
   */
  public async saveCommand(command: CommandCard): Promise<void> {
    const presentIndex = this.commands.findIndex(s => s.uuid === command.uuid);
    const newCommand = Object.assign({}, command);

    newCommand.connection = this.cards.find(c => c.uuid === newCommand.connectionId);

    if (presentIndex > -1) {
      this.commands[presentIndex] = Object.assign(this.commands[presentIndex], newCommand);
    } else {
      newCommand.uuid = v4();
      this.commands.push(newCommand);
    }

    this.saveData();
  }

  /**
   * Opens the shortcut creation form used to edit or create a new shortcut model.
   * If no shortcut model is provided then the form will create a new one.
   * 
   * @param shortcut optional parameter that defines which shortcut should be edited, if none then create a new shortcut.
   */
  public openShortcutForm(shortcut?: ShortcutCard): void {
    const modal = this.modalService.show(ShortcutFormComponent);
    modal.content.card = shortcut || new ShortcutCard();
    modal.content.card = Object.assign({}, modal.content.card);
    modal.content.connections = this.cards;

    modal.content.saveCard
      .pipe(first())
      .subscribe(newShortcut => {
        modal.hide();
        this.saveShortcut(newShortcut)
      });
  }

  /**
   * Opens the command creation form used to edit or create a new command model.
   * If no command model is provided then the form will create a new one.
   * 
   * @param command optional parameter that defines which command should be edited, if none then create a new command.
   */
  public openCommandForm(command?: CommandCard): void {
    const modal = this.modalService.show(CommandFormComponent);
    modal.content.card = command || new CommandCard();
    modal.content.card = Object.assign({}, modal.content.card);
    modal.content.connections = this.cards;

    modal.content.saveCard
      .pipe(first())
      .subscribe(newCommand => {
        modal.hide();
        this.saveCommand(newCommand)
      });
  }

  /**
   * Opens the card creation form used to edit or create a new card model.
   * If no card model is provided then the form will create a new one.
   * 
   * @param card optional parameter that defines which card should be edited, if none then create a new card.
   */
  public openCardForm(card?: ConnectionCard): void {
    const modal = this.modalService.show(CardFormComponent);
    modal.content.card = card || new ConnectionCard();
    modal.content.card = Object.assign({}, modal.content.card);

    modal.content.saveCard
      .pipe(first())
      .subscribe(newCard => {
        modal.hide();
        this.saveCard(newCard)
      });
  }
}