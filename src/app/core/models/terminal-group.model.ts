import { TerminalColumn } from './terminal-column.model';
import { TerminalModel } from './terminal.model';

export class TerminalGroup {
  name: string;
  columns: TerminalColumn[] = [];

  constructor(terminal?: TerminalModel) {
    if (terminal) {
      this.name = terminal.name;
      this.columns.push(new TerminalColumn(terminal));
    }
  }
}
