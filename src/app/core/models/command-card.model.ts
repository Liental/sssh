import { MinLength } from 'class-validator';
import { ConnectionCard } from './connection-card.model';

export class CommandCard {
  uuid?: string;

  @MinLength(1, { message: 'Provided name is empty' })
  name: string;

  @MinLength(1, { message: 'Provided location is invalid' })
  location: string = '~';

  connectionId: string = '';

  @MinLength(1, { message: 'Provided command is invalid' })
  command: string = '';
  
  connection?: ConnectionCard;

  lastUsed?: Date;
}
