import { IsNumber, MinLength } from 'class-validator';

export class ConnectionCard {
  uuid?: string;

  @MinLength(1, { message: 'Provided name is empty' })
  name: string;

  @MinLength(1, { message: 'Provided address is empty' })
  address: string;

  @MinLength(1, { message: 'Provided username is empty' })
  username: string;

  @MinLength(1, { message: 'Provided password is empty' })
  password: string;

  @IsNumber({}, { message: 'Provided port is invalid' })
  port: number = 22;

  lastConnection: Date;
}
