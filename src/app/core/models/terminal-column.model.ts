import { TerminalModel } from './terminal.model';

export class TerminalColumn {
  terminals: TerminalModel[] = [];
  width: number = 1;

  constructor(terminal: TerminalModel) {
    if (terminal) {
      this.terminals.push(terminal);
    }
  }
}
