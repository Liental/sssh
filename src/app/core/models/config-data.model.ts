import { CommandCard } from './command-card.model';
import { ConnectionCard } from './connection-card.model';
import { ShortcutCard } from './shortcut-card.model';

export class ConfigData {
  shortcuts: ShortcutCard[] = [];
  commands: CommandCard[] = [];
  connections: ConnectionCard[] = [];
}