import { ConnectionCard } from './connection-card.model';

export class TerminalModel {
  name: string;
  isLocal: boolean;
  card: ConnectionCard;
  width: number = 100;
  pwd?: string;
  cmd?: string;
  isFileTransfer?: boolean;
}