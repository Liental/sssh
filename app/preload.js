const customTitlebar = require('custom-electron-titlebar');

window.addEventListener('DOMContentLoaded', () => {
  const titleBar = new customTitlebar.Titlebar({
    backgroundColor: customTitlebar.Color.fromHex('#F3F1F4'),
    icon: './favicon.ico'
  });

  titleBar.updateMenu(null);

  window['titleBar'] = titleBar;
});